/*
 * Routes.h
 *
 *  Created on: May 19, 2019
 *      Author: aksha
 */

#ifndef ROUTES_H_
#define ROUTES_H_

checkpoint_t routes_src_dest[18] = {
                                {37.337546, -121.881793, 0}, // src of route 1
                                {37.3371917,-121.881330, 1}, // dest of route 1
                                {37.337546, -121.881793, 2}, // src of route 2
                                {37.3371917,-121.881330, 3}, // dest of route 2
                                {37.337546, -121.881793, 4},
                                {37.3371917,-121.881330, 5},
                                {37.337546, -121.881793, 6},
                                {37.3371917,-121.881330, 7},
                                {37.337546, -121.881793, 8},
                                {37.3371917,-121.881330, 9},
                                {37.337546, -121.881793, 10},
                                {37.3371917,-121.881330, 11},
                                {37.337546, -121.881793, 12},
                                {37.3371917,-121.881330, 13},
                                {37.337546, -121.881793, 14},
                                {37.3371917,-121.881330, 15},
                                {37.337546, -121.881793, 16},
                                {37.3371917,-121.881330, 17},
};

checkpoint_t route_1[20] = {
                            {37.335477, -121.882885, 0}, // Tower building end
                            {37.335353, -121.883269, 1}, // Clark building start
                            {37.335939, -121.882572, 2}, // Clark entrance
                            {37.335955, -121.882569, 3}, // Central classroom building
                            {37.336167, -121.882036, 4}, // Clark end
                            {37.336128, -121.881840, 5}, // Clark end 1
                            {37.336233, -121.881772, 6}, // Clark end 2
                            {37.336511, -121.881807, 7}, // ISSS entrance
                            {37.336506, -121.881805, 8}, // Engg 189 entrance
                            {37.336648, -121.881476, 9}, // Engg 189 ent mid
                            {37.336688, -121.881385, 10}, // Engg building last entrance
                            {37.336814, -121.880966, 11}, // Student Union cafeteria entrance
                            {37.336964, -121.880919, 12}, // SJSU welcome center ent
                            {37.336994, -121.880539, 13}, // Spartan book store
                            {37.337125, -121.880063, 14} // Wells fargo
                        };


checkpoint_t route_2[20] = {
                            {37.335477, -121.882885, 0}, // Tower building end
                            {37.335353, -121.883269, 1}, // Clark building start
                            {37.335939, -121.882572, 2}, // Clark entrance
                            {37.335955, -121.882569, 3}, // Central classroom building
                            {37.336167, -121.882036, 4}, // Clark end
                            {37.336128, -121.881840, 5}, // Clark end 1
                            {37.336233, -121.881772, 6}, // Clark end 2
                            {37.336511, -121.881807, 7}, // ISSS entrance
                            {37.336506, -121.881805, 8}, // Engg 189 entrance
                            {37.336648, -121.881476, 9}, // Engg 189 ent mid
                            {37.336688, -121.881385, 10}, // Engg building last entrance
                            {37.336814, -121.880966, 11}, // Student Union cafeteria entrance
                            {37.336964, -121.880919, 12}, // SJSU welcome center ent
                            {37.336994, -121.880539, 13}, // Spartan book store
                            {37.337125, -121.880063, 14}, // Industrial studies
                            {37.337125, -121.880063, 15} // Wells fargo
                        };

checkpoint_t route_3[20] = {
                            {37.335477, -121.882885, 0}, // Tower building end
                            {37.335353, -121.883269, 1}, // Clark building start
                            {37.335939, -121.882572, 2}, // Clark entrance
                            {37.335955, -121.882569, 3}, // Central classroom building
                            {37.336167, -121.882036, 4}, // Clark end
                            {37.336128, -121.881840, 5}, // Clark end 1
                            {37.336233, -121.881772, 6}, // Clark end 2
                            {37.336511, -121.881807, 7}, // ISSS entrance
                            {37.336506, -121.881805, 8}, // Engg 189 entrance
                            {37.336648, -121.881476, 9}, // Engg 189 ent mid
                            {37.336688, -121.881385, 10}, // Engg building last entrance
                            {37.336814, -121.880966, 11}, // Student Union cafeteria entrance
                            {37.336964, -121.880919, 12}, // SJSU welcome center ent
                            {37.336994, -121.880539, 13}, // Spartan book store
                            {37.337125, -121.880063, 14}, // Industrial studies
                            {37.337125, -121.880063, 15} // Wells fargo
                        };

checkpoint_t route_4[20] = {
                            {37.335477, -121.882885, 0}, // Tower building end
                            {37.335353, -121.883269, 1}, // Clark building start
                            {37.335939, -121.882572, 2}, // Clark entrance
                            {37.335955, -121.882569, 3}, // Central classroom building
                            {37.336167, -121.882036, 4}, // Clark end
                            {37.336128, -121.881840, 5}, // Clark end 1
                            {37.336233, -121.881772, 6}, // Clark end 2
                            {37.336511, -121.881807, 7}, // ISSS entrance
                            {37.336506, -121.881805, 8}, // Engg 189 entrance
                            {37.336648, -121.881476, 9}, // Engg 189 ent mid
                            {37.336688, -121.881385, 10}, // Engg building last entrance
                            {37.336814, -121.880966, 11}, // Student Union cafeteria entrance
                            {37.336964, -121.880919, 12}, // SJSU welcome center ent
                            {37.336994, -121.880539, 13}, // Spartan book store
                            {37.337125, -121.880063, 14}, // Industrial studies
                            {37.337125, -121.880063, 15} // Wells fargo
                        };

checkpoint_t route_5[20] = {
                            {37.335477, -121.882885, 0}, // Tower building end
                            {37.335353, -121.883269, 1}, // Clark building start
                            {37.335939, -121.882572, 2}, // Clark entrance
                            {37.335955, -121.882569, 3}, // Central classroom building
                            {37.336167, -121.882036, 4}, // Clark end
                            {37.336128, -121.881840, 5}, // Clark end 1
                            {37.336233, -121.881772, 6}, // Clark end 2
                            {37.336511, -121.881807, 7}, // ISSS entrance
                            {37.336506, -121.881805, 8}, // Engg 189 entrance
                            {37.336648, -121.881476, 9}, // Engg 189 ent mid
                            {37.336688, -121.881385, 10}, // Engg building last entrance
                            {37.336814, -121.880966, 11}, // Student Union cafeteria entrance
                            {37.336964, -121.880919, 12}, // SJSU welcome center ent
                            {37.336994, -121.880539, 13}, // Spartan book store
                            {37.337125, -121.880063, 14}, // Industrial studies
                            {37.337125, -121.880063, 15} // Wells fargo
                        };


checkpoint_t route_6[20] = {
                            {37.335477, -121.882885, 0}, // Tower building end
                            {37.335353, -121.883269, 1}, // Clark building start
                            {37.335939, -121.882572, 2}, // Clark entrance
                            {37.335955, -121.882569, 3}, // Central classroom building
                            {37.336167, -121.882036, 4}, // Clark end
                            {37.336128, -121.881840, 5}, // Clark end 1
                            {37.336233, -121.881772, 6}, // Clark end 2
                            {37.336511, -121.881807, 7}, // ISSS entrance
                            {37.336506, -121.881805, 8}, // Engg 189 entrance
                            {37.336648, -121.881476, 9}, // Engg 189 ent mid
                            {37.336688, -121.881385, 10}, // Engg building last entrance
                            {37.336814, -121.880966, 11}, // Student Union cafeteria entrance
                            {37.336964, -121.880919, 12}, // SJSU welcome center ent
                            {37.336994, -121.880539, 13}, // Spartan book store
                            {37.337125, -121.880063, 14}, // Industrial studies
                            {37.337125, -121.880063, 15} // Wells fargo
                        };


checkpoint_t route_7[20] = {
                            {37.335477, -121.882885, 0}, // Tower building end
                            {37.335353, -121.883269, 1}, // Clark building start
                            {37.335939, -121.882572, 2}, // Clark entrance
                            {37.335955, -121.882569, 3}, // Central classroom building
                            {37.336167, -121.882036, 4}, // Clark end
                            {37.336128, -121.881840, 5}, // Clark end 1
                            {37.336233, -121.881772, 6}, // Clark end 2
                            {37.336511, -121.881807, 7}, // ISSS entrance
                            {37.336506, -121.881805, 8}, // Engg 189 entrance
                            {37.336648, -121.881476, 9}, // Engg 189 ent mid
                            {37.336688, -121.881385, 10}, // Engg building last entrance
                            {37.336814, -121.880966, 11}, // Student Union cafeteria entrance
                            {37.336964, -121.880919, 12}, // SJSU welcome center ent
                            {37.336994, -121.880539, 13}, // Spartan book store
                            {37.337125, -121.880063, 14}, // Industrial studies
                            {37.337125, -121.880063, 15} // Wells fargo
                        };

checkpoint_t route_8[20] = {
                            {37.335477, -121.882885, 0}, // Tower building end
                            {37.335353, -121.883269, 1}, // Clark building start
                            {37.335939, -121.882572, 2}, // Clark entrance
                            {37.335955, -121.882569, 3}, // Central classroom building
                            {37.336167, -121.882036, 4}, // Clark end
                            {37.336128, -121.881840, 5}, // Clark end 1
                            {37.336233, -121.881772, 6}, // Clark end 2
                            {37.336511, -121.881807, 7}, // ISSS entrance
                            {37.336506, -121.881805, 8}, // Engg 189 entrance
                            {37.336648, -121.881476, 9}, // Engg 189 ent mid
                            {37.336688, -121.881385, 10}, // Engg building last entrance
                            {37.336814, -121.880966, 11}, // Student Union cafeteria entrance
                            {37.336964, -121.880919, 12}, // SJSU welcome center ent
                            {37.336994, -121.880539, 13}, // Spartan book store
                            {37.337125, -121.880063, 14}, // Industrial studies
                            {37.337125, -121.880063, 15} // Wells fargo
                        };

checkpoint_t route_9[20] = {
                            {37.335477, -121.882885, 0}, // Tower building end
                            {37.335353, -121.883269, 1}, // Clark building start
                            {37.335939, -121.882572, 2}, // Clark entrance
                            {37.335955, -121.882569, 3}, // Central classroom building
                            {37.336167, -121.882036, 4}, // Clark end
                            {37.336128, -121.881840, 5}, // Clark end 1
                            {37.336233, -121.881772, 6}, // Clark end 2
                            {37.336511, -121.881807, 7}, // ISSS entrance
                            {37.336506, -121.881805, 8}, // Engg 189 entrance
                            {37.336648, -121.881476, 9}, // Engg 189 ent mid
                            {37.336688, -121.881385, 10}, // Engg building last entrance
                            {37.336814, -121.880966, 11}, // Student Union cafeteria entrance
                            {37.336964, -121.880919, 12}, // SJSU welcome center ent
                            {37.336994, -121.880539, 13}, // Spartan book store
                            {37.337125, -121.880063, 14}, // Industrial studies
                            {37.337125, -121.880063, 15} // Wells fargo
                        };

#endif /* ROUTES_H_ */
