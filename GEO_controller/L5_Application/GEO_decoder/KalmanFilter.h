/*
 * KalmanFilter.h
 *
 *  Created on: May 19, 2019
 *      Author: aksha
 */

#ifndef KALMANFILTER_H_
#define KALMANFILTER_H_

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

float kalmanFilterOut(float z_measured);


#endif /* KALMANFILTER_H_ */
