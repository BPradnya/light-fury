/*
 * KalmanFilter.c
 *
 *  Created on: May 19, 2019
 *      Author: aksha
 */

/** A simple kalman filter example by Adrian Boeing
 www.adrianboeing.com
 */

#include "KalmanFilter.h"

//initial values for the kalman filter
float x_est_last = 0;
float P_last = 0;
//the noise in the system
float Q = 0.322;
float R = 0.617;

float K;
float P;
float P_temp;
float x_temp_est;
float x_est;
float z_measured; //the 'noisy' value we measured
//float z_real = 0; //the ideal value we wish to measure

float kalmanFilterOut(float z_value)
{
        //do a prediction
        x_temp_est = x_est_last;
        P_temp = P_last + Q;
        //calculate the Kalman gain
        K = P_temp * (1.0/(P_temp + R));
        //correct
        x_est = x_temp_est + K * (z_measured - x_temp_est);
        P = (1- K) * P_temp;
        //we have our new system
        printf("Mesaured position: %6.3f\n",z_measured);
        //printf("Kalman   position: %6.3f \n",x_est);

        //update our last's
        P_last = P;
        x_est_last = x_est;

        return x_est;
}

/*
int main()
{
      float mX = 1;
  while(mX < 10)
  {

    printf("Kalman returns %f\n",kalmanFilterOut(mX) );
    mX++;
  }
  return 0;
}
*/



