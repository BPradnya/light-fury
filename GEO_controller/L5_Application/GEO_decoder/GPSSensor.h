/*
 * GPSSensor.h
 *
 *  Created on: Apr 17, 2019
 *      Author: aksha
 */

#ifndef GPSSENSOR_H_
#define GPSSENSOR_H_


#include <stdbool.h>

#include <stdlib.h>
#include "../c_code/c_uart3.h"

#include "../c_code/c_gpio.h"

#include "stdint.h"
#include "string.h"
#include "stdio.h"

#include "../periodic_scheduler/periodic_callback.h"
#include "../../L3_Utils/printf_lib.h"
#include "math.h"


typedef struct checkpoint_t{
    float lat_val;
    float long_val;
    int nIdx;
}checkpoint_t;

// Debug
void setSourceDebug(double  lati, double longi);
void setDestinationDebug(double  lati, double longi);

uint8_t computeNearestRoute(void);
void computeCheckpointsPool(checkpoint_t *route);
void printCheckpointsPool(void);

double getCompassAngle(void);

double getLati(void);

double getLongi(void);

bool getDestinationSetFlag(void);

uint8_t getCurrentCheckpointIdx(void);

//double getCurrentCheckpointLat(void);
//double getCurrentCheckpointLong(void);
checkpoint_t getCurrentCheckpoint(void);

double getCurrentRadius(void);

void setSource(void);

void setDestination(double, double);

bool checkDestinationSentAgain(void);
void destToBeSetAgain(void);

//void computeNearestCheckpoint(checkpoint_t , checkpoint_t, checkpoint_t*);

checkpoint_t computeNearestCheckpoint(checkpoint_t , checkpoint_t , uint8_t);

bool setRoute(void);
void printRoute(void);

double getTurningAngle(void);

void setCurrentCheckpoint(int );
bool gpsUartInit(unsigned int, int, int);
void gpsLedLockInit(void);
bool gpsInit(void);
void transaction_4hz(void);
void gpsDebug(void);
bool checkGpslock(void);
void getGpsCompassValue(void);
bool checkIfCheckpointReached(void);
bool checkIfCarIsInCircle(void);
bool checkIfDestinationReached(void);
double computeDistance(double sourceLatitude, double sourceLongitude, double destinationLatitude, double destinationLongitude);
double computeBearingAngle(double sourceLatitude, double sourceLongitude, double destinationLatitude, double destinationLongitude);
void navigate(void);

#endif /* GPSSENSOR_H_ */
