/*
 * GPSSensor.c
 *
 *  Created on: Apr 17, 2019
 *      Author: aksha
 */
#include "GPSSensor.h"

#include "GeoDecoder.h"
#include "KalmanFilter.h"
#include "Routes.h"

#include <stdbool.h>

#include <stdlib.h>
#include "../c_code/c_uart3.h"
#include "../c_code/can_ops.h"
#include "../c_code/c_gpio.h"

#include "stdint.h"
#include "string.h"
#include "stdio.h"

#include "../periodic_scheduler/periodic_callback.h"
#include "../../L3_Utils/printf_lib.h"
#include "math.h"

//#define SINGLE_CHECKPOINT 1
#define BAUD_RATE 115200
#define UART_QUEUE_SIZE 118

#define RADIUS 2
#define COMPASS_OFFSET 0
#define ROUTES_LEN 9
#define NO_OF_CHECKPOINTS 4

#define PI  3.1432
#define PI_DEGREES 180
#define ALGO_1 1  // midpoint algo
#define ALGO_2 0  // select route and compute checkpoints
#define ALGO_3 0  // hardcore checkpoints


double compass = 0;
double longitude = 0;
double latitude = 0;
double bearingAngle = 0;
double turningAngle = 0;

static checkpoint_t current_checkpoint;

static checkpoint_t source = {37.337665, -121.881391, 0};
static checkpoint_t destination = {37.336330, -121.892711, 10};

double current_radius = 0;
static uint8_t last_checkpoint_set_idx = 0;

static bool neg_flag = false;
static bool dest_flag = false;
static bool src_flag = false;
static bool dest_set_flag = false;
static bool route_set = false;
//static bool reverse_routing = false;


// to be removed
bool debug_flag = true;


/*static checkpoint_t chkpoints_pool[11] = {
                {37.337665, -121.881391, 0},
                {37.337547, -121.881298, 1},
                {37.337418, -121.881192, 2},
                {37.337266, -121.881101, 3},
                {37.337225, -121.881049, 4},
                {37.337244, -121.880935, 5},
                {37.337292, -121.880804, 6},
                {37.338210, -121.879131, 7},
                {37.337750, -121.878868, 8},
                {37.337446, -121.878570, 9},
                {37.338091, -121.881356, 10}
            };*/

/*



    lat :37.339646
    long :-121.880708
    comp :359.325963
    sat : 13

 * */
static checkpoint_t chkpoints_pool[38]  = {//{37.336330, -121.892711, 0},

        {37.338892, -121.880815, 0},{37.339062,-121.880866, 1},{37.339155, -121.880854, 2},
        {37.339310, -121.880956, 3}, {37.339353, -121.881043, 4},
        {37.339617, -121.881346, 6},{37.339681, -121.881246, 7},{37.339681, -121.881246, 8},
        {37.339741, -121.881121, 9},{37.339795, -121.880992, 10},{37.339851, -121.880875, 11},
        {37.339885, -121.880740, 12},{37.339674, -121.880588, 13},{37.339532, -121.880475, 14},
        {37.339387, -121.880361, 15},{37.339227, -121.880247, 16},{37.339092, -121.880155, 17},
        {37.338963, -121.880166, 18},{37.338883, -121.880328, 19},{37.338802, -121.880468, 20},
        {37.338740, -121.880608, 21},{37.338785, -121.880756, 22},{37.338905, -121.880642, 23},
        {37.339020, -121.880728, 24},{37.339071, -121.880605, 25},{37.339222, -121.880720, 26},
        {37.339372, -121.880830, 27},{37.339538, -121.880956, 28},{37.339475, -121.881101, 29},
        {37.339475, -121.881101, 30},{37.339633, -121.880883, 31},{37.339477, -121.880773, 32},
        {37.339295, -121.880634, 33},{37.339115, -121.880514, 34},{37.339162, -121.880375, 35},
        {37.339334, -121.880489, 36},{37.339487, -121.880595, 37},{37.339646, -121.880708, 38}
};

/*
 * checkpoints are idx:  0, lat : 37.338894, long : -121.880814
The checkpoints are idx:  1, lat : 37.338783, long : -121.880753
The checkpoints are idx:  2, lat : 37.338905, long : -121.880646
The checkpoints are idx:  3, lat : 37.339020, long : -121.880730
The checkpoints are idx:  4, lat : 37.338921, long : -121.880859
 *
 * */


/*
static checkpoint_t chkpoints_pool[6]  = {//{37.336330, -121.892711, 0},
        {37.338852, -121.880837, 0},
        {37.338926, -121.880673, 1},
                                {37.338867, -121.880780, 2}, // 339 corner
                                //{37.338926, -121.880673, 2}, // 329A
                                {37.339042, -121.880757, 3}, // 331 corner
                                {37.339008, -121.880875, 4}, // 339 front
                                {37.337311, -121.881660, 5}};

*/

#if ALGO_1
static checkpoint_t chkpoints[NO_OF_CHECKPOINTS] = {{37.338852, -121.880837, 0},
                                {37.338867, -121.880780, 1}, // 339 corner
                                {37.338926, -121.880673, 2}, // 329A
                                {37.339042, -121.880757, 3}, // 331 corner
                                //{37.339008, -121.880875, 4} // 339 front
                                //{37.337311, -121.881660, 5}
};

#endif

#if ALGO_3
static checkpoint_t chkpoints[NO_OF_CHECKPOINTS] = {{0,0,0}/*, // 339 corner
                                {37.338926, -121.880673, 2}, // 329A
                                {37.339042, -121.880757, 3}*/
};
#endif


// Logic for setting route
/* 1) Select route depending on the destination
 * 2) Compare the checkpoints with source to find the nearest checkpoint to source and dest
 * 3) Form a checkpoints array
*/


double getCompassAngle(void)
{
    //return kalmanFilterOut(compass);
    return compass;
}

double getTurningAngle(void)
{
    return turningAngle;
}

double getLati(void)
{
    return latitude;
}

double getLongi(void)
{
    return longitude;
}

/*
uint8_t getCurrentCheckpointIdx(void)
{
    return current_checkpoint.nIdx;
}
*/

double getCurrentRadius(void)
{
    return current_radius;
}

checkpoint_t getCurrentCheckpoint(void)
{
    return current_checkpoint;
}

/*double getCurrentCheckpointLong(void)
{
    return current_checkpoint.long_val;
}*/

bool getDestinationSetFlag(void)
{
    return dest_set_flag;
}

#if ALGO_2
uint8_t computeNearestRoute(void)
{
    uint32_t distArr[100] = {0};
    int route = 0;
    int j = 0;
    for(int i = 0; i < (sizeof(routes_src_dest)/sizeof(routes_src_dest[0])); i=i+2)
    {
        float srcDist1 =  computeDistance(source.lat_val, source.long_val, routes_src_dest[i].lat_val, routes_src_dest[i].long_val);

        float srcDist2 = computeDistance(routes_src_dest[i+1].lat_val, routes_src_dest[i+1].long_val, source.lat_val, source.long_val);

        float destDist1 =  computeDistance(destination.lat_val, destination.long_val, routes_src_dest[i].lat_val, routes_src_dest[i].long_val);

        float destDist2 = computeDistance(routes_src_dest[i+1].lat_val, routes_src_dest[i+1].long_val, destination.lat_val, destination.long_val);

        distArr[j] = (srcDist1+destDist1+srcDist2+destDist2);
        j++;
    }

    float minDist = distArr[0];

    for(int i=1; i<j; i++)
    {
       if(distArr[i] < minDist)
       {
           //u0_dbg_printf("mindist for %d is %d\n",i, distArr[i]);
           minDist = distArr[i];
           route = i;
       }
    }
    //u0_dbg_printf("inside %d \n >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", route);
       return route;
}

void computeNearestCheckpoint(checkpoint_t src, checkpoint_t dest, checkpoint_t *route)
{

       float distSrc[20] = {0.0};
       float distDest[20] = {0.0};

       //u0_dbg_printf("HELOLOLEOELEOELEOELEOELEOELEEOELELEL : %d\n", sizeof(route));
       for(int i = 0; i < 20; i++)
       {
          float srcDist =  computeDistance(src.lat_val, src.long_val, route[i].lat_val, route[i].long_val);

          float destDist = computeDistance(route[i].lat_val, route[i].long_val, dest.lat_val, dest.long_val);

          distSrc[i] = srcDist;
          distDest[i] = destDist;
       }

       float min_src = distDest[0];
       float min_dest = distDest[0];
       int min_src_idx = 0;
       int min_dest_idx = 0;
       for(int i=1; i<(sizeof(distDest)/sizeof(distDest[0])); i++)
       {
           if(distSrc[i] < min_src)
                      {
                          min_src = distSrc[i];
                          min_src_idx = i;
                      }
           if(distDest[i] < min_dest)
           {
               min_dest = distDest[i];
               min_dest_idx = i;
           }
       }
       int arr_len = 0;
       if(min_src_idx > min_dest_idx)
       {
           reverse_routing = true;
           arr_len = min_src_idx - min_dest_idx;
       }
       else
       {
           arr_len = min_dest_idx - min_src_idx;
       }

       int k = 0;
       for(k = 0; k < arr_len; k++)
       {
           if(reverse_routing){
               chkpoints[k] = route[min_src_idx - k];
           }
           else
           {
               chkpoints[k] = route[min_src_idx + k];
           }
       }
       chkpoints[k] = destination;
}

void computeCheckpointsPool(checkpoint_t *route)
{
    computeNearestCheckpoint(source, destination, route);
    //printCheckpointsPool();
    route_set = true;
}

void printCheckpointsPool(void)
{
    for(int i = 0; i < 18; i++){
        u0_dbg_printf("the elements are: %.7f, %.7f\n",chkpoints[i].lat_val, chkpoints[i].long_val);
        //u0_dbg_printf("the elements are: %.7f, %.7f\n",route[i].lat_val, route[i].long_val);

    }
    u0_dbg_printf("All elements printed\n");
}

bool setRoute(void)
{
    int route_chosen = 99;
    bool checkpoint_pool_set = false;
    if(src_flag && dest_set_flag && !route_set)
    {
        route_chosen = computeNearestRoute();
        switch(route_chosen){
            case 0: computeCheckpointsPool(route_1);
                    //checkpoint_pool_set = true;
                    break;
            case 1: computeCheckpointsPool(route_2);
                    //checkpoint_pool_set = true;
                    break;
            case 2: computeCheckpointsPool(route_3);
                    //checkpoint_pool_set = true;
                    break;
            case 3: computeCheckpointsPool(route_4);
                    //checkpoint_pool_set = true;
                    break;
            case 4: computeCheckpointsPool(route_5);
                    //checkpoint_pool_set = true;
                    break;
            case 5: computeCheckpointsPool(route_6);
                    //checkpoint_pool_set = true;
                    break;
            case 6: computeCheckpointsPool(route_7);
                    //checkpoint_pool_set = true;
                    break;
            case 7: computeCheckpointsPool(route_8);
                    //checkpoint_pool_set = true;
                    break;
            case 8: computeCheckpointsPool(route_9);
                    //checkpoint_pool_set = true;
                    break;
            default:break;
        }
        checkpoint_pool_set = true;
    }
    return checkpoint_pool_set;
}
#endif

#if ALGO_1
checkpoint_t computeNearestCheckpoint(checkpoint_t src, checkpoint_t dest, uint8_t checkpoint_idx)
{
       double distArr[(sizeof(chkpoints_pool)/sizeof(chkpoints_pool[0]))] = {0.0};

       //float small3Arr[3] = {0.0};

       for(int i = 0; i < ((int)sizeof(chkpoints_pool)/(int)sizeof(chkpoints_pool[0])); i++)
       {
          float srcDist =  computeDistance(src.lat_val, src.long_val, chkpoints_pool[i].lat_val, chkpoints_pool[i].long_val);

          float destDist = computeDistance(chkpoints_pool[i].lat_val, chkpoints_pool[i].long_val, dest.lat_val, dest.long_val);

          distArr[i] = (srcDist+destDist);
          //u0_dbg_printf(" source : %.6f, %.6f\n", src.lat_val, src.long_val);
          //u0_dbg_printf(" destination : %.6f, %.6f\n", dest.lat_val, dest.long_val);
          //u0_dbg_printf(" checkpoint : %.6f, %.6f\n", chkpoints_pool[i].lat_val, chkpoints_pool[i].long_val);
          ////u0_dbg_printf("The dist for iteration %d for index %d is : %f\n", checkpoint_idx, i, distArr[i]);
       }

       double minDist = distArr[0];

       chkpoints[checkpoint_idx].lat_val  = chkpoints_pool[0].lat_val;
       chkpoints[checkpoint_idx].long_val = chkpoints_pool[0].long_val;
       for(int i=1; i<((int)sizeof(distArr)/(int)sizeof(distArr[0])); i++)
       {
           if(distArr[i] < minDist)
           {
               minDist = distArr[i];
               last_checkpoint_set_idx = i;
               chkpoints[checkpoint_idx].lat_val  = chkpoints_pool[i].lat_val;
               chkpoints[checkpoint_idx].long_val = chkpoints_pool[i].long_val;
           }
       }
       chkpoints_pool[last_checkpoint_set_idx].lat_val = 37;
       chkpoints_pool[last_checkpoint_set_idx].long_val = -121;
       return chkpoints[checkpoint_idx];
}

bool setRoute(void)
{
    if(src_flag && dest_set_flag && !route_set)
    {
        checkpoint_t src = source;
        checkpoint_t dest = destination;
        int i = 0;
        for(i = 0; i < NO_OF_CHECKPOINTS - 1; i++){
            src = computeNearestCheckpoint(src, dest, i);
        }
        route_set = true;
    }
    return route_set;
}

void printRoute(void)
{
    for(int i = 0; i < NO_OF_CHECKPOINTS; i++ )
    {
        printf("The checkpoints are idx:  %d, lat : %f, long : %f \n", i, chkpoints[i].lat_val, chkpoints[i].long_val);
    }
}

#endif

void gpsDebug()
{
    u0_dbg_printf("lat :%.6f\n", latitude);
    u0_dbg_printf("long :%.6f\n", longitude);
    u0_dbg_printf("comp :%f \n", compass);
    u0_dbg_printf("sat : %d\n", satellites);
}

void setDestinationDebug(double  lati, double longi)
{
    dest_set_flag = true;
        // values will come from BT
        destination.lat_val = lati;
        destination.long_val = longi;
        //destination.nIdx = 6;
        chkpoints[NO_OF_CHECKPOINTS-1].lat_val = lati;
        chkpoints[NO_OF_CHECKPOINTS-1].long_val = longi;
        // indicates destination reception from BT
        ledOn(4);
}

void setDestination(double  lati, double longi)
{
    // values will come from BT

    destination.lat_val = lati;
    destination.long_val = longi;

    chkpoints[NO_OF_CHECKPOINTS-1].lat_val = lati;
    chkpoints[NO_OF_CHECKPOINTS-1].long_val = longi;

    // indicates destination reception from BT
    ledOn(4);
    dest_set_flag = true;
}

void setSourceDebug(double  lati, double longi)
{
    // values will come from BT
    source.lat_val = lati;
    source.long_val = longi;
    source.nIdx = 0;
    current_checkpoint.lat_val = lati;
    current_checkpoint.long_val = longi;
    current_checkpoint.nIdx = 0;
    src_flag = true;
}

void setSource(void)
{
    // values will come from BT
    source.lat_val = latitude;
    source.long_val = longitude;
    source.nIdx = 0;
    current_checkpoint.lat_val = latitude;
    current_checkpoint.long_val = longitude;
    current_checkpoint.nIdx = 0;
    src_flag = true;
}

void setCurrentCheckpoint(int chkpnt_idx)
{
    // values will come from BT
    current_checkpoint.lat_val = chkpoints[chkpnt_idx-1].lat_val;
    current_checkpoint.long_val = chkpoints[chkpnt_idx-1].long_val;
    current_checkpoint.nIdx = chkpnt_idx;
}

bool gpsUartInit(unsigned baud_rate, int rx_buf, int txbuf)
{
    if(baud_rate == BAUD_RATE)
    {
        return uart3_init(baud_rate, rx_buf, txbuf);
    }
    return false;
}

void gpsLedLockInit(void)
{
    setPinPortAsOutput(1, 0);
}

bool gpsInit(void)
{
    gpsLedLockInit();
    return gpsUartInit(BAUD_RATE, UART_QUEUE_SIZE, 1);
}

bool gpsLocationInit(void)
{
    return true;
}

/*Function for Gps lock*/
bool checkGpslock(void)
{
    if (isLocked())
    {
        if(!src_flag){
            getGpsCompassValue();
            setSource();
        }
        ledOn(1);
        return true;
    }
    else
    {
        ledOff(1);
        return false;
    }
}

bool checkIfDestinationReached(void)
{
    current_radius = computeDistance(latitude, longitude, destination.lat_val, destination.long_val);
    if(current_radius < RADIUS){
        dest_flag = true;
    }
    else
    {
        dest_flag = false;
    }
    return dest_flag;
}

bool checkIfCheckpointReached(void)
{
    bool checkpoint_reached = false;
    current_radius = computeDistance(latitude, longitude, current_checkpoint.lat_val, current_checkpoint.long_val);
    /*if(debug_flag){
        u0_dbg_printf("lat1: %.6f\n", latitude);
        u0_dbg_printf("long1: %.6f\n", longitude);
        u0_dbg_printf("lat2: %.6f\n", current_checkpoint.lat_val);
        u0_dbg_printf("long2: %.6f\n", current_checkpoint.long_val);
        u0_dbg_printf("curRad: %.3f\n", current_radius);
    }*/

    if(current_radius < RADIUS)
    {
        checkpoint_reached = true;
/*if(current_checkpoint.nIdx == 1)
{
    debug_flag = false;
}*/
        ////u0_dbg_printf("checkpoint reached : %d\n", current_checkpoint.nIdx);
        // code for setting next checkpoint
        int temp_count = ++current_checkpoint.nIdx;
        setCurrentCheckpoint(temp_count);
    }
    return checkpoint_reached;
}

double computeTurningAngle(void)
{
    bearingAngle = computeBearingAngle(latitude, longitude, current_checkpoint.lat_val, current_checkpoint.long_val);
    double turn_angle = bearingAngle - (compass + COMPASS_OFFSET);
    if (turn_angle>180)
    {
        turn_angle=turn_angle-360;
    }

    if (turn_angle<-180)
    {
        turn_angle=turn_angle+360;
    }

    if(turn_angle < 0)
    {
        neg_flag = true;
    }
    else
    {
        neg_flag = false;
    }

    return turn_angle;
}


double computeDistance(double sourceLatitude, double sourceLongitude, double destinationLatitude, double destinationLongitude)
{
    double theta, distance;
    theta = sourceLongitude - destinationLongitude;
    distance = sin(degreesToRadians(sourceLatitude)) * sin(degreesToRadians(destinationLatitude)) + cos(degreesToRadians(sourceLatitude)) * cos(degreesToRadians(destinationLatitude)) * cos(degreesToRadians(theta));
    distance = acos(distance);
    distance = radiansToDegrees(distance);
    distance = distance * (double)60 * (double)1.1515;
    distance = distance * (double)1609.344; // from miles to meters
    ////u0_dbg_printf("The Distance is: %f\n", distance);
    return distance;
}

double computeBearingAngle(double sourceLatitude, double sourceLongitude, double destinationLatitude, double destinationLongitude)
{
     double lat1=sourceLatitude;
     double lon1=sourceLongitude;
     double lat2=destinationLatitude;
     double lon2=destinationLongitude;
     double bearing = 0 , lon_difference = 0;
     lat1 = (lat1 * PI)/ PI_DEGREES;
     lon1 = (lon1 * PI)/ PI_DEGREES;
     lat2 = (lat2 * PI)/ PI_DEGREES;
     lon2 = (lon2 * PI)/ PI_DEGREES;
     lon_difference = lon2 - lon1;
     bearing = atan2((sin(lon_difference)*cos(lat2)), ((cos(lat1)*sin(lat2))-(sin(lat1)*cos(lat2)*cos(lon_difference)))) ;
     bearing = (bearing * PI_DEGREES)/ PI;
     if( bearing <= 0 )
     {
         bearing += 360;
     }
     return bearing;
}

bool checkDestinationSentAgain(void)
{
    bool dest_again_set = false;
     if(readSwitch(1))
     {
         destToBeSetAgain();
         ledOff(4);
         dest_again_set = true;
     }
     return dest_again_set;
}

void destToBeSetAgain(void)
{
    dest_flag = false;
    src_flag = false;
    route_set = false;
}

void navigate(void)
{
    current_radius = computeDistance(latitude, longitude, destination.lat_val, destination.long_val);
    turningAngle = computeTurningAngle();
    /*if(debug_flag){
        u0_dbg_printf("TurnAng : %.6f\n", turningAngle);
    }*/
    if(!dest_flag)
    {
        dest_flag = (current_radius < RADIUS);
        if(getFixType()){
            canTurningAngleMsgTx(turningAngle, dest_flag, neg_flag);
        }
    }
}

void getGpsCompassValue(void)
{
    char byte = 0;
    while(uart3_getchar(&byte,0))
    {
        int receive = byte;
        uint8_t decodedMessage = decode(receive);
        switch (decodedMessage)
        {
            case NAZA_MESSAGE_GPS_TYPE:
            {
                /*Every 250ms GPS values*/
                longitude     = getLongitude();
                latitude      = getLatitude();
                break;

            }
            case NAZA_MESSAGE_MAGNETOMETER_TYPE:
            {
                /*Every 30 ms compass value is coming*/
                compass = getHeading();
                break;
            }
            default:
                break;
        }
    }
}
