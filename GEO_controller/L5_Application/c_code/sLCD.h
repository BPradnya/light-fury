/*
 * sLCD.h
 *
 *  Created on: Apr 19, 2019
 *      Author: MAVERICK
 */

#ifndef SLCD_H_
#define SLCD_H_

#include <stdbool.h>
#include "c_uart2.h"
#include <stdint.h>

struct uart
{
    int uart_portNo;
};

/*Basic Functions*/
bool initLCD(void);
void displayOnLCD(void);
bool isLCD_Ok(void);
void clrLCD(void);

/*Advance Functions*/


#endif /* SLCD_H_ */
