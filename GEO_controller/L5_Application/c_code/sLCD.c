/*
 * sLCD.c
 *
 *  Created on: Apr 19, 2019
 *      Author: MAVERICK
 */

#include <stdio.h>
#include "sLCD.h"
#include "c_uart2.h"
#include "string.h"
#include "printf_lib.h"
#include "../GEO_decoder/GPSSensor.h"

#define LCD_INIT_CMD                        0xF0
//#define LCD_RESPONSE_CMD                    "$"

char display[48] = "";
char data = 0xF0;

extern double compass;
extern double longitude;
extern double latitude;

#define UART_TIMEOUT_MS                     0

bool initLCD(void)
{
    uart2_init(9600, 64, 1);
    return uart2_putchar(data, UART_TIMEOUT_MS);
}

void displayOnLCD(void)
{
    //sprintf(display,"%.6f, %.6f, %u", latitude, longitude, compass);
    uart2_putline(display, 0);
}

/*
bool isLCD_Ok(void)
{
    char uart_rbuf;

    if(!uart2_getchar(&uart_rbuf, UART_TIMEOUT_MS))
    {
        return true;
    }
    return false;
}

void clrLCD(void)
{

}*/

