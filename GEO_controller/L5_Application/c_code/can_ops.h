/*
 * funCan.h
 *
 *  Created on: Apr 16, 2019
 *      Author: MAVERICK
 */

#ifndef CAN_OPS_H_
#define CAN_OPS_H_

#include <stdint.h>
#include <stdbool.h>
#include "can.h"
#include "common_header.h"

bool dbc_app_send_can_msg(uint32_t mid, uint8_t dlc, uint8_t bytes[8]);
void canInit(void);
void canBusReset(can_t can);
void isCanOff(void);
bool canHBTx(void);
bool canDebugMsgTx(void);
bool canGeoBTDataTx(void);
bool canGeoBTLatLongTx(void);
bool canGeoBTCompTx(void);
bool canCheckpointBTTx(void);
bool canTurningAngleMsgTx(double, bool, bool);
void canRx(can_t can);



#endif /* CAN_OPS_H_ */
