/**
 * @file
 *
 * The purpose of this "C" callbacks is to provide the code to be able
 * to call pure C functions and unit-test it in C test framework
 */
#include <c_code/can_ops.h>
#include <stdint.h>
#include <stdbool.h>
#include "c_uart3.h"
#include "printf_lib.h"
#include "GEO_decoder/GPSSensor.h"
#include "sLCD.h"
#include "geo_hb.h"

#define UART_RX_QUEUE_SIZE (12*3+64+18)
#define DEBUG 1

bool flag = false;

bool C_period_init(void) {
    canInit();
    gpsInit();
    /*while(!flag)
    {
        //setDestinationDebug(37.339642, -121.881440);
        //setSourceDebug(37.338892, -121.880815);
        canRx(can1);
        checkGpslock();
        flag = setRoute();
        //printRoute();
    }*/
    return true;
}
bool C_period_reg_tlm(void) {
    return true;
}

void C_period_1Hz(uint32_t count) {
    (void) count;
    isCanOff();
}

void C_period_10Hz(uint32_t count) {
    (void) count;
    //getGpsCompassValue();
    //checkGpslock();
    // Without destination car must not be started
    // debug geo calculations
    // computeDistance(37.335939, -121.882572, 37.335805, -121.882437);
    // computeBearingAngle(37.335939, -121.882572, 37.335805, -121.882437);
    // printGeoCalculations()

    // Things to be printed on app
    // route no.
    // fix type
    // lock
    // turning angle
    // current checkpoint
    // current radius

    //if(!flag)
    //{
        canRx(can1);
        //getGpsCompassValue();
        checkGpslock();
        if(!flag){
            flag = setRoute();
            //printRoute();
        }
    //}
    //else
    //{
        flag = !checkDestinationSentAgain();
        //checkGpslock();
    /*if(!flag){
    setDestinationDebug(37.339642, -121.881440);
           setSourceDebug(37.338892, -121.880815);
    flag = true;
    }*/
        getGpsCompassValue();
        checkIfCheckpointReached();
        navigate();
        canGeoBTLatLongTx();
        canGeoBTCompTx();
        //canCheckpointBTTx();
    //}
}

void C_period_100Hz(uint32_t count) {
    (void) count;
    geoHB();
    /*getGpsCompassValue();
    canGeoBTLatLongTx();
    canGeoBTCompTx();*/
}

void C_period_1000Hz(uint32_t count) {
    (void) count;
}
