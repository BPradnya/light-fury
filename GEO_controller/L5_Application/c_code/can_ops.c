/*
 * can_ops.c
 *
 *  Created on: Apr 16, 2019
 *      Author: Akshay
 */

#include "../c_code/can_ops.h"
#include <string.h>
#include <stdbool.h>
#include "GEO_decoder/GeoDecoder.h"
#include "GEO_decoder/GPSSensor.h"
#include "printf_lib.h"
#include "../c_code/c_io.h"

#define POS_THRESHOLD 15
#define NEG_THRESHOLD -15

bool dest_rx_from_BT_flag = false;

static LF_GEO_BT_LAT_LONG_t gps_val = {0};
static LF_GEO_BT_COMP_t compass_fix_val = {0};
static LF_GEO_DATA_t geo_data = {0};
static LF_bridge_dest_t destination_info = {0};
static LF_CHECKPOINT_BT_LAT_LONG_t curr_checkpoint = {0};

bool dbc_app_send_can_msg(uint32_t mid, uint8_t dlc, uint8_t bytes[8])
{
    can_msg_t can_msg = { 0 };
    can_msg.msg_id                = mid;
    can_msg.frame_fields.data_len = dlc;
    memcpy(can_msg.data.bytes, bytes, dlc);
    return CAN_tx(can1, &can_msg, 0);
}

void canInit(void)
{
    if(CAN_init(can1,100,100,32,(void*)canBusReset,NULL))
    {
        CAN_bypass_filter_accept_all_msgs();

        canBusReset(can1);
    }
}

void canBusReset(can_t can)
{
    CAN_reset_bus(can);
}

void isCanOff(void)
{
    if(CAN_is_bus_off(can1))
    {
        canBusReset(can1);
    }
}

bool canHBTx(void)
{
    LF_GEO_HB_t geoHeartBeat = {0};
    geoHeartBeat.geo_hb = 1;
    return dbc_encode_and_send_LF_GEO_HB(&geoHeartBeat);
}

bool canTurningAngleMsgTx(double turn_angle, bool dest_flag, bool neg_flag)
{
    if(dest_flag)
    {
        ld_setnumber(stop);
        geo_data.magnitude_geo = turn_angle;
        geo_data.direction_geo_enum = stop;
    }
    else if(NEG_THRESHOLD < turn_angle && turn_angle < POS_THRESHOLD)
    {
        ld_setnumber(3);
        geo_data.magnitude_geo = turn_angle;
        geo_data.direction_geo_enum = straight;
    }
    else if(neg_flag)
    {
        ld_setnumber(2);
        geo_data.magnitude_geo = (-1)*turn_angle;
        geo_data.direction_geo_enum = left;
    }
    else if(!neg_flag)
    {
        ld_setnumber(1);
        geo_data.magnitude_geo = turn_angle;
        geo_data.direction_geo_enum = right;
    }

    return dbc_encode_and_send_LF_GEO_DATA(&geo_data);

}

bool canGeoBTLatLongTx(void)
{
    enum FixType fix_check = getFixType();
    //if((fix_check == FIX_2D || fix_check == FIX_3D) && checkGpslock()){
    if(fix_check == FIX_2D || fix_check == FIX_3D){
        gps_val.GEO_BT_lat_debug = getLati();
        gps_val.GEO_BT_long_debug = getLongi();
    }
    else{
        gps_val.GEO_BT_lat_debug = 0;
        gps_val.GEO_BT_long_debug = 0;
    }
    return dbc_encode_and_send_LF_GEO_BT_LAT_LONG(&gps_val);
}

bool canGeoBTCompTx(void)
{
        compass_fix_val.GEO_BT_comp_debug = getCompassAngle();
        compass_fix_val.GPS_FIX = getFixType();
        compass_fix_val.GPS_LOCK = isLocked();
        compass_fix_val.GEO_BT_turning_angle_debug = getTurningAngle();
        compass_fix_val.CURRENT_CHECKPOINT = (getCurrentCheckpoint()).nIdx;
        compass_fix_val.CURRENT_RADIUS = getCurrentRadius();
        return dbc_encode_and_send_LF_GEO_BT_COMP(&compass_fix_val);
}

bool canCheckpointBTTx(void)
{
    curr_checkpoint.CHECKPOINT_BT_lat_debug = (getCurrentCheckpoint()).lat_val;
    curr_checkpoint.CHECKPOINT_BT_long_debug = (getCurrentCheckpoint()).long_val;
    return dbc_encode_and_send_LF_CHECKPOINT_BT_LAT_LONG(&curr_checkpoint);
}

void canRx(can_t can) //LF_BLUETOOTH
{
    can_msg_t can_msg;

    while(CAN_rx(can,&can_msg,0) && !dest_rx_from_BT_flag)
    {
        dbc_msg_hdr_t can_msg_hdr;

        can_msg_hdr.dlc = can_msg.frame_fields.data_len;
        can_msg_hdr.mid = can_msg.msg_id;

        if(dbc_decode_LF_bridge_dest(&destination_info,can_msg.data.bytes, &can_msg_hdr)){
            setDestination(destination_info.dest_lat, destination_info.dest_long);
        }
    }
}






