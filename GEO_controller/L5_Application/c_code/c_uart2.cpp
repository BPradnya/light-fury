#include "c_uart2.h"
#include "uart2.hpp"

// Assuming UART2 is already initialized
// You may want to also add uart2_init() in C

bool uart2_init(unsigned int baudRate, int rxQSize, int txQSize) {
    return Uart2::getInstance().init(baudRate, rxQSize, txQSize);
}

bool uart2_getchar(char *byte, uint32_t timeout_ms) {
    return Uart2::getInstance().getChar(byte, timeout_ms);
}

bool uart2_gets(char *byte, uint32_t max, uint32_t timeout_ms) {
    return Uart2::getInstance().gets(byte, max, timeout_ms);
}

bool uart2_putchar(char byte, uint32_t timeout_ms) {
    return Uart2::getInstance().putChar(byte, timeout_ms);
}

void uart2_putline(char *byte, uint32_t timeout_ms) {
    Uart2::getInstance().putline(byte, timeout_ms);
}
