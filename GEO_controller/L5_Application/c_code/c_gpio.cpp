/*
 * c_gpio.cpp
 *
 *  Created on: Mar 5, 2019
 *      Author: gaura
 */


#include "c_gpio.h"
#include "LabGPIO.hpp"
#include "io.hpp"
#include "printf_lib.h"


bool readSwitch(int pinNum)
{
    return SW.getSwitch(pinNum);
}

bool setPinPortAsOutput(int pinNum, int portNum)
{
    LabGPIO gp(pinNum, portNum);
    return gp.setAsOutput();
}

void setResetPinPort(int pinNum, int portNum){
    // Object should be created once
    LabGPIO gp(pinNum, portNum);
    gp.setAsOutput();
    gp.setHigh();
}

void ledSet(int ledNum, bool on)
{
    LE.set(ledNum, on);
}

bool ledOn(int ledNum){
    LE.on(ledNum);
    return true;
}

bool ledOff(int ledNum){
    LE.off(ledNum);
    return true;
}
