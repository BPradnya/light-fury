
#include "c_uart3.h"
#include "uart3.hpp"

// Assuming UART3 is already initialized
// You may want to also add uart2_init() in C

bool uart3_init(unsigned int baudRate, int rxQSize, int txQSize) {
    return Uart3::getInstance().init(baudRate, rxQSize, txQSize);
}

bool uart3_getchar(char *byte, uint32_t timeout_ms) {
    return Uart3::getInstance().getChar(byte, timeout_ms);
}

bool uart3_gets(char *byte, uint32_t max, uint32_t timeout_ms) {
    return Uart3::getInstance().gets(byte, max, timeout_ms);
}

bool uart3_putchar(char byte, uint32_t timeout_ms) {
    return Uart3::getInstance().putChar(byte, timeout_ms);
}

/*QueueHandle_t uart3_getRxQueue() {
    return Uart3::getInstance().getRxQueue();
}*/


