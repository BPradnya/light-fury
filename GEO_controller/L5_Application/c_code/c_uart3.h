#ifndef UART3_C_H__
#define UART3_C_H__
#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

bool uart3_init(unsigned int baudRate, int rxQSize, int txQSize);
bool uart3_getchar(char *byte, uint32_t timeout_ms);
bool uart3_gets(char *byte, uint32_t max, uint32_t timeout_ms);
bool uart3_puchar(char byte, uint32_t timeout_ms);


#ifdef __cplusplus
}
#endif
#endif /* UART3_C_H__ */
