/*
 * c_io.h
 *
 *  Created on: Feb 18, 2019
 *      Author: aksha
 */

#ifndef IO_C_H__
#define IO_C_H__
#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

void ld_setnumber(unsigned int number);

unsigned int light_sensor(void);

#ifdef __cplusplus
}
#endif
#endif /* IO_C_H__ */

