/*
 * c_io.cpp
 *
 *  Created on: Feb 18, 2019
 *      Author: akshay
 */

#include "c_io.h"
#include "io.hpp"
#include "LED_Display.hpp"

// Assuming UART2 is already initialized
// You may want to also add uart2_init() in C

// Light Sensor

unsigned int light_sensor(void){
    return Light_Sensor::getInstance().getPercentValue();
}

// LED Display Set Number
void ld_setnumber(unsigned int number) {
    LED_Display::getInstance().setNumber(number);
}




