/**
 * @file
 *
 * The purpose of this "C" callbacks is to provide the code to be able
 * to call pure C functions and unit-test it in C test framework
 */

#include "stddef.h"
#include "c_period_callbacks.h"
#include "can.h"
#include "Can_Operation.h"
#include "c_led_display.h"
#include "switch_led.h"



bool C_period_init(void) {

#if LIGHTFURY
    c_led_display_init();
    c_led_display_clear();
    LEDInit();
    canInit();
#endif

    return true;
}


bool C_period_reg_tlm(void) {
    return true;
}


void C_period_1Hz(uint32_t count) {
    (void) count;

#if LIGHTFURY

    isCanOff();

#endif
}



void C_period_10Hz(uint32_t count) {
    (void) count;

#if LIGHTFURY

#endif
}


void C_period_100Hz(uint32_t count) {

#if LIGHTFURY
    can_transmit_master_hb();

    can_rx_all_messages();

    populate_motor_status();

    can_transmit_motor_command();

#endif
}


void C_period_1000Hz(uint32_t count) {
    (void) count;
}

