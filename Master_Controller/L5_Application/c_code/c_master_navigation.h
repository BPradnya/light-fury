/*
 * c_master_navigation.h
 *
 *  Created on: Apr 20, 2019
 *      Author: prita
 */

#ifndef C_MASTER_NAVIGATION_H_
#define C_MASTER_NAVIGATION_H_

#include "Can_Operation.h"
#include "generated_can.h"

/*
-------------------------------------------------------------
|   Driver  |   Servo   |   Geo         |   Name            |
-------------------------------------------------------------
|     6     |     F     |   GEO_STRT    |   movFrwd         |
|     4     |     SL    |   GEO_LEFT    |   movSoftLeft     |
|     3     |     ML    |   GEO_LEFT    |   movModLeft      |
|     4     |     SR    |   GEO_RIGHT   |   movSoftRight    |
|     3     |     MR    |   GEO_RIGHT   |   movModRight     |
|     0     |     F     |   GEO_STOP    |   movStop         |
-------------------------------------------------------------
*/

#define MAX_OBSTACLE_DISTANCE                       35
#define MIN_HALT_DISTANCE                           55                  //For front sensor

#define GEO_SOFT_TURN                               20
#define GEO_MOD_TURN                                60

#define GEO_STOP                                    4
#define GEO_STRT                                    3
#define GEO_LEFT                                    2
#define GEO_RIGHT                                   1
#define GEO_NO_DATA                                 0

#define LED_OBSTACLE_STOP                           99
#define LED_LEFT                                    10
#define LED_RIGHT                                   01
#define LED_FORWARD                                 11
#define LED_REVERSE                                 88
#define LED_SIGNAL_STOP                             69
#define LED_NO_SIGNAL                               77

#define FORCE_REV                                   15

LF_MOTOR_CMD_t motor_cmd_decision(LF_SENSOR_DATA_t *sensor_data_rx,  LF_GEO_DATA_t *geo_data_rx);


#endif /* C_MASTER_NAVIGATION_H_ */
