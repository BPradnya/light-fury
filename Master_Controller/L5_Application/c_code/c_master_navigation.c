/*
 * c_master_navigation.c
 *
 *  Created on: Apr 20, 2019
 *      Author: prita
 */

#include "c_master_navigation.h"
#include "c_led_display.h"
#include "stdint.h"

LF_MOTOR_CMD_t motor_cmd = {0};

static bool flagReverse = false;

void motorModRight(void)
{
    motor_cmd.MOTOR_CMD_steer_enum = steer_moderate_right;
    motor_cmd.MOTOR_CMD_drive_enum = drive_slow;

    c_led_display_set_number(LED_RIGHT);
}

void motorStop(void)
{
    motor_cmd.MOTOR_CMD_steer_enum = steer_front;
    motor_cmd.MOTOR_CMD_drive_enum = drive_stop;

    c_led_display_set_number(LED_OBSTACLE_STOP);
}
void motorReverse(void)
{
    motor_cmd.MOTOR_CMD_steer_enum = steer_moderate_left;
    motor_cmd.MOTOR_CMD_drive_enum = drive_reverse;

    c_led_display_set_number(LED_REVERSE);
}

void motorSoftLeft(void)
{
    motor_cmd.MOTOR_CMD_steer_enum = steer_soft_left;
    motor_cmd.MOTOR_CMD_drive_enum = drive_moderate;

    c_led_display_set_number(LED_LEFT);
}

void motorModLeft(void)
{
    motor_cmd.MOTOR_CMD_steer_enum = steer_moderate_left;
    motor_cmd.MOTOR_CMD_drive_enum = drive_slow;

    c_led_display_set_number(LED_LEFT);
}

void motorSoftRight(void)
{
    motor_cmd.MOTOR_CMD_steer_enum = steer_soft_right;
    motor_cmd.MOTOR_CMD_drive_enum = drive_moderate;

    c_led_display_set_number(LED_RIGHT);
}

void motorForward(void)
{
        motor_cmd.MOTOR_CMD_steer_enum = steer_front;
        motor_cmd.MOTOR_CMD_drive_enum = drive_forward;

        c_led_display_set_number(LED_FORWARD);
}


bool USmovFrwd(LF_SENSOR_DATA_t *sensor_data_rx)
{
    /*************move forward**********/ /**********us sensor conditions 0 0 0 *************/
    if(sensor_data_rx->left_sensor > MAX_OBSTACLE_DISTANCE
            && sensor_data_rx->front_sensor > MAX_OBSTACLE_DISTANCE
            && sensor_data_rx->right_sensor > MAX_OBSTACLE_DISTANCE )
    {
        motorForward();
        return true;
    }

    /*************move forward when obstacle on both side************/ /**********us sensor conditions 1 0 1 *************/
    else if(sensor_data_rx->left_sensor < MAX_OBSTACLE_DISTANCE
            && sensor_data_rx->front_sensor > MAX_OBSTACLE_DISTANCE
            && sensor_data_rx->right_sensor < MAX_OBSTACLE_DISTANCE )
    {
        motorForward();
        return true;
    }

    return false;
}

bool GEOmovFrwd( LF_GEO_DATA_t *geo_data_rx)
{
    /*************move forward with geo data < 30************/ /**********geo sensor conditions 00 mag *************/
    if(geo_data_rx->direction_geo_enum == GEO_STRT
            && geo_data_rx->magnitude_geo < GEO_SOFT_TURN )
    {
        motorForward();
        return true;
    }

    return false;
}

bool USmovSoftLeft(LF_SENSOR_DATA_t *sensor_data_rx)
{
    /*************move softLeft**********/ /**********us sensor conditions 0 0 1 *************/
    if(sensor_data_rx->left_sensor > MAX_OBSTACLE_DISTANCE
            && sensor_data_rx->front_sensor > MAX_OBSTACLE_DISTANCE
            && sensor_data_rx->right_sensor < MAX_OBSTACLE_DISTANCE)
    {
        motorSoftLeft();
        return true;
    }
    return false;
}

bool GEOmovSoftLeft(LF_GEO_DATA_t *geo_data_rx)
{
    /*************move softLeft with geo 60< data < 30************/ /**********geo sensor conditions 10 mag *************/
    if(geo_data_rx->direction_geo_enum == GEO_LEFT
            && geo_data_rx->magnitude_geo >= GEO_SOFT_TURN
            && geo_data_rx->magnitude_geo < GEO_MOD_TURN )
    {
        motorSoftLeft();
        return true;
    }

    return false;

}
//
bool USmovModLeft(LF_SENSOR_DATA_t *sensor_data_rx)
{
    /*************movemod left**********/ /**********sensor conditions 0 1 1 *************/
    if(sensor_data_rx->left_sensor > MAX_OBSTACLE_DISTANCE
            && sensor_data_rx->front_sensor < MAX_OBSTACLE_DISTANCE
            && sensor_data_rx->right_sensor < MAX_OBSTACLE_DISTANCE)
    {
        motorModLeft();
        return true;
    }
    return false;
}

bool GEOmovModLeft( LF_GEO_DATA_t *geo_data_rx)
{
    /*************move softLeft with data > 60************/ /**********geo sensor conditions 10 mag *************/
    if(geo_data_rx->direction_geo_enum == GEO_LEFT && geo_data_rx->magnitude_geo >= GEO_MOD_TURN  )
    {
        motorModLeft();
        return true;
    }
    return false;
}
//
bool USmovSoftRight(LF_SENSOR_DATA_t *sensor_data_rx)
{
    /*************move soft right**********/ /**********geo sensor conditions 1 0 0 *************/
    if(sensor_data_rx->left_sensor < MAX_OBSTACLE_DISTANCE && sensor_data_rx->front_sensor > MAX_OBSTACLE_DISTANCE && sensor_data_rx->right_sensor > MAX_OBSTACLE_DISTANCE)
    {
        motorSoftRight();
        return true;
    }
    return false;
}

bool GEOmovSoftRight( LF_GEO_DATA_t *geo_data_rx)
{
    /*************move softLeft with 30 < data > 60************/ /**********geo sensor conditions 01 mag *************/
    if(geo_data_rx->direction_geo_enum == GEO_RIGHT
            && geo_data_rx->magnitude_geo >= GEO_SOFT_TURN
            && geo_data_rx->magnitude_geo < GEO_MOD_TURN  )
    {
        motorSoftRight();
        return true;
    }
    return false;
}

bool USmovModRight(LF_SENSOR_DATA_t *sensor_data_rx)
{

    /*************move reverse **********/ /**********sensor conditions 0 1 0*************/
    if( sensor_data_rx->left_sensor > MAX_OBSTACLE_DISTANCE
            && sensor_data_rx->right_sensor > MIN_HALT_DISTANCE
            && sensor_data_rx->front_sensor < MAX_OBSTACLE_DISTANCE)
    {
        motorModRight();
        return true;
    }
    /*************move mod right**********/ /**********sensor conditions 1 1 0 *************/
    else if(sensor_data_rx->left_sensor < MAX_OBSTACLE_DISTANCE
            && sensor_data_rx->front_sensor < MAX_OBSTACLE_DISTANCE
            && sensor_data_rx->right_sensor > MAX_OBSTACLE_DISTANCE)
    {
        motorModRight();
        return true;
    }
    return false;
}

bool GEOmovModRight( LF_GEO_DATA_t *geo_data_rx)
{
    /*************move softLeft with data > 60************/ /**********geo sensor conditions 01 mag *************/
    if(geo_data_rx->direction_geo_enum == GEO_RIGHT && geo_data_rx->magnitude_geo >= GEO_MOD_TURN  )
    {
        motorModRight();
        return true;
    }

    return false;
}

bool GEOmovStop( LF_GEO_DATA_t *geo_data_rx)
{
    if(geo_data_rx->direction_geo_enum == GEO_STOP )
    {
        motorStop();
        return true;
    }

    return false;
}
//
bool USmovReverse(LF_SENSOR_DATA_t *sensor_data_rx)
{

    /*************move reverse **********/ /**********sensor conditions 1 1 1*************/
    if(sensor_data_rx->left_sensor < MAX_OBSTACLE_DISTANCE
                && sensor_data_rx->right_sensor < MAX_OBSTACLE_DISTANCE
                && sensor_data_rx->front_sensor < MAX_OBSTACLE_DISTANCE+10)
    {
        motorReverse();
        return true;
    }

    return false;
}

// LCD should not be part of this function
LF_MOTOR_CMD_t motor_cmd_decision(LF_SENSOR_DATA_t *sensor_data_rx, LF_GEO_DATA_t *geo_data_rx)
{

    if(flagReverse)
    {
        if(sensor_data_rx->front_sensor > MIN_HALT_DISTANCE+10
                && sensor_data_rx->right_sensor > MAX_OBSTACLE_DISTANCE+5
                && sensor_data_rx->left_sensor > MAX_OBSTACLE_DISTANCE+5)
        {
            flagReverse = false;
        }
        else
        {
            motorReverse();
        }
    }
    else if( USmovReverse(sensor_data_rx) )
    {
        flagReverse = true;
    }
    else if( GEOmovStop(geo_data_rx))
    {

    }
    else if( USmovModLeft(sensor_data_rx) || USmovModRight(sensor_data_rx) || USmovSoftLeft(sensor_data_rx) || USmovSoftRight(sensor_data_rx) )
    {

    }
    else if( GEOmovModLeft(geo_data_rx) || GEOmovModRight(geo_data_rx) || GEOmovSoftLeft(geo_data_rx) || GEOmovSoftRight(geo_data_rx) )
    {

    }
    else if( USmovFrwd(sensor_data_rx) || GEOmovFrwd(geo_data_rx))
    {

    }
    return motor_cmd;
}

