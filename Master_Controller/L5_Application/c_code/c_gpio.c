/*
 * c_gpio.c
 *
 *  Created on: Apr 16, 2019
 *      Author: Niraj
 */

#include <stdbool.h>
#include "c_gpio.h"
#include "LPC17xx.h"

void c_gpio_SetAsInput(const struct GPIO_struct* ptr)
{
    if(ptr->port == 0)
    {
        LPC_GPIO0->FIODIR &= ~(1<<ptr->pin);
    }
    else if(ptr->port == 2)
    {
        LPC_GPIO2->FIODIR &= ~(1<<ptr->pin);
    }
}

void c_gpio_SetAsOutput(const struct GPIO_struct* ptr)
{
    if(ptr->port == 0)
    {
        LPC_GPIO0->FIODIR |= (1<<ptr->pin);
    }
    else if(ptr->port == 2)
    {
        LPC_GPIO2->FIODIR |= (1<<ptr->pin);
    }
}

bool c_gpio_read(const struct GPIO_struct* ptr)
{
    bool status = false;
    if(ptr->port == 0)
    {
        status = (LPC_GPIO0->FIOPIN & (1<<ptr->pin));
    }
    else if(ptr->port == 2)
    {
        status = (LPC_GPIO2->FIOPIN & (1<<ptr->pin));
    }
    return status;
}

void c_gpio_setHigh(const struct GPIO_struct *ptr)
{
    if(ptr->port == 0)
    {
         LPC_GPIO0->FIOSET = (1<<ptr->pin);
    }
    else if(ptr->port == 2)
    {
        LPC_GPIO2->FIOSET = (1<<ptr->pin);
    }
}

void c_gpio_setLow(const struct GPIO_struct *ptr)
{
    if(ptr->port == 0)
    {
         LPC_GPIO0->FIOCLR = (1<<ptr->pin);
    }
    else if(ptr->port == 2)
    {
        LPC_GPIO2->FIOCLR = (1<<ptr->pin);
    }
}

void c_gpio_set(const struct GPIO_struct *ptr, bool on)
{
    on == true ? c_gpio_setHigh(ptr) : c_gpio_setLow(ptr);
}

void c_gpio_toggle(const struct GPIO_struct *ptr)
{
    c_gpio_read(ptr) ? c_gpio_setLow(ptr) : c_gpio_setHigh(ptr);
}

/*
 * Wrappers to call in applications during struct initialization
 * Usage Example:
 *  GPIO_struct P2_1 =  {2,1,_setAsIn,_setAsOut,
 *                          _readstatus,_setHigh,_setLow,
 *                          _set,_toggle
 *                      };
 */
void _setAsIn(const struct GPIO_struct* ptr)
{
    c_gpio_SetAsInput(ptr);
}

void _setAsOut(const struct GPIO_struct* ptr)
{
    c_gpio_SetAsOutput(ptr);
}

bool _readstatus(const struct GPIO_struct* ptr)
{
    return c_gpio_read(ptr);
}

void _setHigh(const struct GPIO_struct* ptr)
{
    c_gpio_setHigh(ptr);
}

void _setLow(const struct GPIO_struct* ptr)
{
    c_gpio_setLow(ptr);
}

void _set(const struct GPIO_struct* ptr,bool on)
{
    c_gpio_set(ptr,on);
}

void _toggle(const struct GPIO_struct* ptr)
{
    c_gpio_toggle(ptr);
}
