/*
 * c_gpio.h
 *
 *  Created on: Mar 5, 2019
 *      Author: gaura
 */


#ifndef C_GPIO_H_
#define C_GPIO_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>


struct GPIO_struct
{
    uint8_t port;
    uint8_t pin;
    void (*set_as_input)(const struct GPIO_struct*);
    void (*set_as_output)(const struct GPIO_struct*);
    bool (*readpin)(const struct GPIO_struct*);
    void (*set_high)(const struct GPIO_struct*);
    void (*set_low)(const struct GPIO_struct*);
    void (*setb)(const struct GPIO_struct*,bool on);
    void (*toggle)(const struct GPIO_struct*);
};


//void* c_gpio_getinstance(void *LPC1758_GPIO_Type_Ptr);
//void c_gpio_setasinput(void* ptr);
//void c_gpio_setoutput(void* ptr);
//
//bool c_gpio_read(void* ptr);
//void c_gpio_setHigh(void* ptr);
//void c_gpio_setLow(void* ptr);
//void c_gpio_set(void);
//void c_gpio_toggle(void);
//
//void c_gpio_enablePullUp(void);
//void c_gpio_enablePullDown(void);
//void c_gpio_disablePullUpPullDown(void);
//void c_gpio_enableOpenDrainMode(bool openDrain);


#ifdef __cplusplus
}
#endif

#endif /* C_GPIO_H_ */
