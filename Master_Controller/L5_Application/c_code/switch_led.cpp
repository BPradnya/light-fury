/*
 * switch_led.cpp
 *
 *  Created on: Mar 3, 2019
 *      Author: kaust
 */

#include "switch_led.h"
#include "io.hpp"
#include "printf_lib.h"

bool readSwitch(int pinNum)
{
    return SW.getSwitch(pinNum);
}

void ledOn(int pinNum)
{
    LE.on(pinNum);
}

void ledOff(int pinNum)
{
    LE.off(pinNum);
}

void ledset(int pinNum,bool state)
{
    state ? LE.on(pinNum) : LE.off(pinNum);
}


void LEDInit(void)
{
    LPC_GPIO2->FIODIR |= (1 << 0);          // for enable on bridge signal
    LPC_GPIO2->FIOPIN &= ~(1 << 0);

    LPC_GPIO1->FIODIR &= ~(1<<9);           //  for gps reset switch

}
