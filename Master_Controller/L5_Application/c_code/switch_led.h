/*
 * switch_led.h
 * switch_led.h
 *
 *  Created on: Mar 3, 2019
 *      Author: kaust
 */

#ifndef SWITCH_LED_H_
#define SWITCH_LED_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>


bool readSwitch(int pinNum);
void ledOn(int pinNum);
void ledOff(int pinNum);
void ledset(int pinNum,bool state);

void LEDInit(void);

#ifdef __cplusplus
}
#endif
#endif

