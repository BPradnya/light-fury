/*
 * Can_tx.c
 *
 *  Created on: Mar 5, 2019
 *      Author: kaust
 */


#include "Can_Operation.h"

static LF_SENSOR_HB_t sensor_msg_hb = {0};
static LF_MOTOR_HB_t motor_msg_hb = {0};
static LF_BRIDGE_HB_t bridge_msg_hb = {0};
static LF_GEO_HB_t geo_msg_hb = {0};

static LF_GEO_DATA_t geo_data_rx = {0};
static LF_SENSOR_DATA_t sensor_data_rx = {0};
static LF_BRIDGE_START_STOP_t start_driving = {0};

static LF_MOTOR_CMD_t motor_cmd = {0};


const uint32_t                             LF_SENSOR_DATA__MIA_MS = 1000;
const LF_SENSOR_DATA_t                     LF_SENSOR_DATA__MIA_MSG = {0};
const uint32_t                             LF_GEO_DATA__MIA_MS = 1000;
const LF_GEO_DATA_t                        LF_GEO_DATA__MIA_MSG = {0};
const uint32_t                             LF_SENSOR_HB__MIA_MS = 1000;
const LF_SENSOR_HB_t                       LF_SENSOR_HB__MIA_MSG = {0};
const uint32_t                             LF_MOTOR_HB__MIA_MS = 1000;
const LF_MOTOR_HB_t                        LF_MOTOR_HB__MIA_MSG = {0};
const uint32_t                             LF_GEO_HB__MIA_MS = 1000;
const LF_GEO_HB_t                          LF_GEO_HB__MIA_MSG = {0};
const uint32_t                             LF_BRIDGE_HB__MIA_MS = 1000;
const LF_BRIDGE_HB_t                       LF_BRIDGE_HB__MIA_MSG = {0};



bool dbc_app_send_can_msg(uint32_t mid, uint8_t dlc, uint8_t bytes[8])
{
    can_msg_t can_msg             = { 0 };
    can_msg.msg_id                = mid;
    can_msg.frame_fields.data_len = dlc;
    memcpy(can_msg.data.bytes, bytes, dlc);
    return CAN_tx(can1, &can_msg, 0);
}

void canInit(void)
{
    if(CAN_init(can1,100,32,32,(void*)canBusReset,NULL))
    {
        canBusReset(can1);
        CAN_bypass_filter_accept_all_msgs();
    }
}

void canBusReset(can_t can)
{
    CAN_reset_bus(can);
}

void isCanOff(void)
{
    if(CAN_is_bus_off(can1))
    {
        canBusReset(can1);
    }
}

void can_transmit_motor_command(void)
{
    dbc_encode_and_send_LF_MOTOR_CMD(&motor_cmd);
}

void can_transmit_master_hb(void)
{
    LF_DRIVER_HB_t driver_hb_msg = {0};
    driver_hb_msg.driver_hb = 1;
    dbc_encode_and_send_LF_DRIVER_HB(&driver_hb_msg);
}

void populate_motor_status() {
    if(start_driving.start_stop_flag == 2)
    {
        motor_cmd = motor_cmd_decision(&sensor_data_rx, &geo_data_rx);
        LPC_GPIO2->FIOPIN |= (1 << 0);
    }
    else if(start_driving.start_stop_flag == 1)
    {
        motor_cmd.MOTOR_CMD_drive_enum= drive_stop;
        motor_cmd.MOTOR_CMD_steer_enum= steer_front;

        c_led_display_set_number(LED_SIGNAL_STOP);
        LPC_GPIO2->FIOPIN &= ~(1 << 0);
    }
    else
    {
        motor_cmd.MOTOR_CMD_drive_enum= drive_stop;
        motor_cmd.MOTOR_CMD_steer_enum= steer_front;

        c_led_display_set_number(LED_NO_SIGNAL);
        LPC_GPIO2->FIOPIN &= ~(1 << 0);
    }

}

void can_rx_all_messages()
{
    can_msg_t can_msg_rx = {0};

    while(CAN_rx(can1,&can_msg_rx,0))
    {
        dbc_msg_hdr_t can_msg_hdr;
        can_msg_hdr.dlc = can_msg_rx.frame_fields.data_len;
        can_msg_hdr.mid = can_msg_rx.msg_id;

        dbc_decode_LF_BRIDGE_START_STOP(&start_driving, can_msg_rx.data.bytes, &can_msg_hdr);

            dbc_decode_LF_SENSOR_DATA(&sensor_data_rx, can_msg_rx.data.bytes, &can_msg_hdr);

            dbc_decode_LF_GEO_DATA(&geo_data_rx, can_msg_rx.data.bytes, &can_msg_hdr);


        if(dbc_decode_LF_SENSOR_HB(&sensor_msg_hb, can_msg_rx.data.bytes, &can_msg_hdr)) {
            ledOn(1);
        }
        if(dbc_decode_LF_MOTOR_HB(&motor_msg_hb, can_msg_rx.data.bytes, &can_msg_hdr)) {
            ledOn(2);
        }
        if(dbc_decode_LF_BRIDGE_HB(&bridge_msg_hb, can_msg_rx.data.bytes, &can_msg_hdr)) {
            ledOn(3);
        }
        if(dbc_decode_LF_GEO_HB(&geo_msg_hb, can_msg_rx.data.bytes, &can_msg_hdr)) {
            ledOn(4);
        }

    }

    dbc_handle_mia_LF_SENSOR_DATA(&sensor_data_rx, 10);

    dbc_handle_mia_LF_GEO_DATA(&geo_data_rx, 10);

    if(dbc_handle_mia_LF_SENSOR_HB(&sensor_msg_hb, 10))
    {
        ledOff(1);
    }
    if(dbc_handle_mia_LF_MOTOR_HB(&motor_msg_hb, 10))
    {
        ledOff(2);
    }
    if(dbc_handle_mia_LF_BRIDGE_HB(&bridge_msg_hb, 10))
    {
        ledOff(3);
    }
    if(dbc_handle_mia_LF_GEO_HB(&geo_msg_hb, 10))
    {
        ledOff(4);
    }

}
