/*
 * Can_tx.h
 *
 *  Created on: Mar 5, 2019
 *      Author: kaust
 */

#ifndef CAN_OPERATION_H_
#define CAN_OPERATION_H_

#include <string.h>
#include "can.h"
#include "switch_led.h"
#include "c_master_navigation.h"
#include "c_led_display.h"
#include "LPC17xx.h"


void canInit(void);
void canBusReset(can_t can);
void isCanOff(void);

void can_transmit_master_hb(void);
void can_transmit_motor_command(void);
void can_rx_all_messages(void);

void populate_motor_status(void);


#endif /* CAN_OPERATION_H_ */
