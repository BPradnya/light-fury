/*
 * compass.hpp
 *
 *  Created on: Mar 22, 2019
 *      Author: Niraj
 */

#ifndef COMPASS_HPP_
#define COMPASS_HPP_

#include <stdint.h>
#include <math.h>
#include "i2c2_device.hpp"

class I2C_Compass: private i2c2_device,public SingletonTemplate<I2C_Compass>
{
public:
    bool compass_init();
//    int16_t get_raw_X_A();
//    int16_t get_raw_Y_A();
//    int16_t get_raw_Z_A();
    int16_t get_raw_X_M();
    int16_t get_raw_Y_M();
    int16_t get_raw_Z_M();
    float get_angle_from_north();
private:
    I2C_Compass():i2c2_device(I2CAddr_CompassMag){}
    friend class SingletonTemplate<I2C_Compass>;  ///< Friend class used for Singleton Template

    typedef enum
    {
//        Ctrl_Reg1_A     = 0x20,
//        Ctrl_Reg4_A     = 0x23,
//        Status_Reg_A    = 0x27,
//        Out_X_A         = 0x28,
//        Out_Y_A         = 0x2a,
//        Out_Z_A         = 0x2c,
        CRA_Reg_M       = 0x00,
        MR_Reg_M        = 0x02,
        Out_X_M         = 0x03,
        Out_Z_M         = 0x05,
        Out_Y_M         = 0x07,

    } __attribute__ ((packed)) Compass_RegMap;
};


#endif /* COMPASS_HPP_ */
