<!-- language: lang-none -->

                     ^    ^
                    / \  //\
      |\___/|      /   \//  .\
      /O  O  \__  /    //  | \ \
     /     /  \/_/    //   |  \  \
     @___@'    \/_   //    |   \   \ 
        |       \/_ //     |    \    \ 
        |        \///      |     \     \ 
       _|_ /   )  //       |      \     _\
      '/,_ _ _/  ( ; -.    |    _ _\.-~        .-~~~^-.
      ,-{        _      `-.|.-~-.           .~         `.
       '/\      /                 ~-. _ .-~      .-~^-.  \
          `.   {            }                   /      \  \
        .----~-.\        \-'                 .~         \  `. \^-.
       ///.----..>    c   \             _ -~             `.  ^-`   ^-_
         ///-._ _ _ _ _ _ _}^ - - - - ~                     ~--,   .-~
                                                               /.-'
Welcome to the repository of our project from the course CMPE-243 offered by Prof.Preetpal Kang at San Jose State University. All reviewed and developed code must be pushed here!
Enjoy!!

* Project Description:-
The project aims to create an elsectric, self driving and Obstacle avoiding car that uses GPS to find its actual destination and uses a smart-phone application to set destination co-ordinates and tracking.

* The project is group project consisting 8 members and the whole team will be divided into following smaller teams:
	1. Sensor Controller team
	2. Motor and I/o Controller team
	3. Geographical Controller team
	4. Master Communication controller team
	5. Android Application team 

* The teams above are divided for Development purpose. Q&A testing will be done by any of the two members. There will be 1 team lead and 2 code reviewers who will perform extra duties along with the development.

* Work completed:
	1. Create Git repository
	2. Git repo join invitation to Preet
	3. Reviewed previous project
	4. Ordered CAN transceiver (MCP2561)


### GENERAL GUIDELINES FOR USING GIT:
1. Clone or pull the Repo in your folder. *Note:* It is good practice to create a branch while working on particular issue and once you make changes in files, create a pull request.
2. Make Changes in files or add new files. 
3. Commit your changes.
4. Just before pushing your code to a branch, do a pull so you always have the lastest code. Resolve merge conflicts, if any.
5. Push your Code.
    
### CONFIGURE EXTERNAL DIFF AND MERGE TOOLS:
*  difftool and mergetool come in very handy for tracking file/directory changes or handling merge conflicts.
*  Install difftool/mergetool that you prefer. KDiff3 is a free tool with pretty decent GUI. [Download KDiff3 from here](http://kdiff3.sourceforge.net/)
*  Once it is installed, go to your project root directory and type folling commands for configuring the tools for git:
    *   `git config --global --add diff.guitool kdiff3`
    *   `git config --global --add difftool.kdiff3.path "C:/Program Files/KDiff3/kdiff3.exe"`
    *   Try if the tool has been configured correctly by using `git difftool` or you can see changes in entire directory in all the files using `git difftool -d`.
    *   *Note:* The command will only work if there ARE changes between commited files and local files.
    *   Similarly configure your mergetool:
    *   `git config --global --add merge.tool kdiff3`
    *   `git config --global --add mergetool.kdiff3.path "C:/Program Files/KDiff3/kdiff3.exe"`
    *   Again, try out if the tool has been configured correctly using `git mergetool`
                
