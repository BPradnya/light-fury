/*
 * rpm.h
 *
 *  Created on: Apr 21, 2019
 *      Author: Niraj
 */

#ifndef RPM_H_
#define RPM_H_

#include "generated_can.h"
#include "eint.h"

#define MS_TO_MPH_FACTOR                            2.23694
#define CM_TO_METER                                 100.0

/*
 * Note: Incrementing rotations per min is left to user of this function
 *       through Intr_callback
 */
bool rpm_init(int port, int pin, eint_intr_t type,void (*Intr_callback)(void));

/*
 * This functions uses interrupts, rising or falling edge,
 * on port and pin defined in rpm_init.
 * This function counts interrupts every second to
 * to get number of rotations per second(rps) return speed in mph.
 * NOTE: THIS NEEDS TO BE CALLED every second (in 1Hz task) and clears
 * rpsIn to zero after calculating mph
 */

void get_rpm(float wheelCircumferenceinCm,float scale, int* rpsIn,float* mph);




#endif /* RPM_H_ */
