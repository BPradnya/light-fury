/*
 * c_adc_val.h
 *
 *  Created on: Apr 6, 2019
 *      Author: Niraj
 */

#ifndef C_ADC_VAL_H_
#define C_ADC_VAL_H_

#include "adc0.h"

#define CH_NO 4

/*  enables input from potentiometer on pin P1.30
 *  This input can either be used for speed or steering
 */
void enable_adc_in_p1_30(void);

float get_reading_forward_motor(void);

float get_reading_backward_motor(void);

float get_reading_servo(void);

#endif /* C_ADC_VAL_H_ */
