/*
 * rpm.c
 *
 *  Created on: Apr 21, 2019
 *      Author: Niraj
 */

#include "rpm.h"

bool rpm_init(int port, int pin, eint_intr_t type,void_func_t Intr_callback)
{
    bool status = false;
    if(0 == port)
    {
        eint3_enable_port0(pin,type,Intr_callback);
        status = true;
    }
    else if (2 == port)
    {
        eint3_enable_port2(pin,type,Intr_callback);
        status = true;
    }
    return status;
}


void get_rpm(float wheelCircumferenceinCm,float scale, int* rpsIn,float* mph)
{
    //printf("RPS:%d\n",*rpsIn);
    *mph = (float)(wheelCircumferenceinCm /CM_TO_METER) * scale * MS_TO_MPH_FACTOR * (*rpsIn);
    *rpsIn=0;
}
