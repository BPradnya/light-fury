/*
 * c_motor_drive.c
 *
 *  Created on: Apr 2, 2019
 *      Author: gaura
 */
#include <stdbool.h>
#include "c_motor_drive.h"

static int rotationsPerSecond;

void Intr_r(void)
{
    rotationsPerSecond++;
}
bool motor_init()
{
    c_led_display_init();
    return (c_pwm_init(Motor, 50) && c_pwm_init(Servo, 50) &&\
            rpm_init(2,6,eint_rising_edge,Intr_r) && \
            pid_init(PROPORTIONAL_CONSTANT,NEUTRAL_DUTY,MAX_FORWARD_DUTY)\
           );

}

//bool steer_left(float percent)
//{
//    return c_pwm_set(Servo,percent);
//}
//
//bool steer_right(float percent){
//    return c_pwm_set(Servo,percent);
//}

bool steer(float percent)
{
    if(percent < MAX_REVERSE_DUTY)
    {
        percent = MAX_REVERSE_DUTY;
    }

    if( percent > MAX_FORWARD_DUTY)
    {
        percent = MAX_FORWARD_DUTY;
    }

    return c_pwm_set(Servo,percent);
}

bool move_forward(Motor_speed speed)
{

    switch(speed)
    {
        case Off:   MotorSpeedStatus.MotorSpeed = NEUTRAL_DUTY;
                    break;
        case Low:   MotorSpeedStatus.MotorSpeed = MOTOR_FW_LOW_SPEED_DUTY;
                    break;
        case Medium:MotorSpeedStatus.MotorSpeed = MOTOR_FW_MEDIUM_SPEED_DUTY;
                    break;
        case High:  MotorSpeedStatus.MotorSpeed = MOTOR_FW_HIGH_SPEED_DUTY;
                    break;
        default:    MotorSpeedStatus.MotorSpeed = NEUTRAL_DUTY;
                    break;
    }

    return c_pwm_set(Motor,MotorSpeedStatus.MotorSpeed);
}

bool move_forward_variable(float speed)
{
    if(speed < NEUTRAL_DUTY)
    {
        speed = NEUTRAL_DUTY;
    }

    if(speed > MAX_FORWARD_DUTY)
    {
        speed = MAX_FORWARD_DUTY;
    }
    MotorSpeedStatus.MotorSpeed = speed;
    return c_pwm_set(Motor,MotorSpeedStatus.MotorSpeed);
}

bool move_backward(void)
{
    dont_move_motor();
    return c_pwm_set(Motor,5);
}

bool move_backward_variable(float speed)
{
    if(speed < MAX_REVERSE_DUTY)
    {
        speed = MAX_REVERSE_DUTY;
    }

    if(speed > NEUTRAL_DUTY)
    {
        speed = NEUTRAL_DUTY;
    }

    MotorSpeedStatus.MotorSpeed = speed;
    return c_pwm_set(Motor,MotorSpeedStatus.MotorSpeed);
}

bool dont_move_motor(void)
{
    return move_forward(Off);
}

bool dont_steer(void)
{
    return c_pwm_set(Servo,NEUTRAL_DUTY);
}

int get_rps(void)
{
    return rotationsPerSecond;
}

void get_motor_rpm_in_mph(float* mph)
{
    get_rpm(WHEEL_CIRCUMFERENCE_CENTIMETERS,SCALE_FACTOR_RPM,&rotationsPerSecond,&MotorSpeedStatus.rpmSpeedInMph);
    if(mph != NULL)
    {
        *mph = MotorSpeedStatus.rpmSpeedInMph;
    }

}

void set_motor_rpm_in_mph(float targetMph, float* getDutyCycle)
{

    pid_setPoint(targetMph,&MotorSpeedStatus.rpmSpeedInMph,&MotorSpeedStatus.MotorSpeed);
    if(getDutyCycle != NULL)
    {
        *getDutyCycle = MotorSpeedStatus.MotorSpeed;
    }
}
