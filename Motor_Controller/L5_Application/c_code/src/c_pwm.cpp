/*
 * c_pwm.cpp
 *
 *  Created on: Apr 2, 2019
 *      Author: gaurav
 */

#include <stddef.h>
#include "c_pwm.h"
#include "lpc_pwm.hpp"

PWM* pwm_dc=NULL;
PWM* pwm_servo=NULL;

bool c_pwm_init(Motor_select pwm, unsigned int freq)
{
   bool status=false;
   if(Motor_select::Motor == pwm)
   {
       pwm_dc = new PWM(PWM::pwm2,freq);
       status = true;
   }
   else if(Motor_select::Servo == pwm)
   {
       pwm_servo = new PWM(PWM::pwm3,freq);
       status = true;
   }
   return status;
}

bool c_pwm_set(Motor_select pwm,float percent)
{
    bool status=false;
    if(Motor_select::Motor == pwm)
      {
         status= pwm_dc->set(percent);
      }
      else if(Motor_select::Servo == pwm)
      {
         status= pwm_servo->set(percent);
      }

    return status;
}

