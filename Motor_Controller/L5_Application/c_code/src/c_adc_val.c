#include "c_adc_val.h"
#include "LPC17xx.h"

void enable_adc_in_p1_30(void)
{
    adc0_init();
    //Set P1.30 as ADC input from potentiometer to vary steering or motor speed.
    LPC_PINCON->PINSEL3 |= (3<<28); // set p1.30 as adc4
    LPC_PINCON->PINMODE3 |= (2<<28); // set NO PULL UP OR PULL DOWN REST
    LPC_ADC->ADCR |= (1<<4); // set bit no. 4 for channel 4
}

float get_reading_forward_motor(void)
{
    return ((adc0_get_reading(CH_NO) * 2.5 / 4096) + 7.5);
}

float get_reading_backward_motor(void)
{
    return (7.5 - (adc0_get_reading(CH_NO) * 2.5 / 4096));
}

float get_reading_servo(void)
{
    return ((adc0_get_reading(CH_NO) * 5.0 / 4096) + 5.0);
}
