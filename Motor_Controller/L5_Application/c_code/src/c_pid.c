/*
 * c_pid.c
 *
 *  Created on: Apr 23, 2019
 *      Author: Niraj
 */

#include "c_pid.h"

static p_t PID_obj;

bool pid_init( float Kp, float minOutputRange, float maxOutputRange)
{
    PID_obj.Kp = Kp;
    PID_obj.minOutputRange = minOutputRange;
    PID_obj.maxOutputRange = maxOutputRange;

    return true;

}

void pid_setPoint( float setPoint,float* currentReading, float* output)
{
    float error = 0;
    error = setPoint - *currentReading;
    *output += PID_obj.Kp * error;
    if(*output <= PID_obj.minOutputRange)
    {
        *output = PID_obj.minOutputRange;
    }
    else if(*output >= PID_obj.maxOutputRange)
    {
        *output = PID_obj.maxOutputRange;
    }
}

