/**
 * @file
 *
 * The purpose of this "C" callbacks is to provide the code to be able
 * to call pure C functions and unit-test it in C test framework
 */
#include <stdio.h>
#include <string.h>
#include "c_period_callbacks.h"

bool dbc_app_send_can_msg(uint32_t mid, uint8_t dlc, uint8_t bytes[8])
{
    can_msg_t can_msg = { 0 };
    can_msg.msg_id                = mid;
    can_msg.frame_fields.data_len = dlc;
    memcpy(can_msg.data.bytes, bytes, dlc);

    return CAN_tx(can1, &can_msg, 0);
}

#define TARGET_STOP_RPM     0
#define TARGET_LOW_RPM      4
#define TARGET_MED_RPM      7
#define TARGET_HIGH_RPM     10

const uint32_t                             LF_MOTOR_CMD__MIA_MS=100;
const LF_MOTOR_CMD_t                       LF_MOTOR_CMD__MIA_MSG={steer_front,drive_stop};
GPIO_struct SW1_P1_9 =   {1,9, _setAsIn,_setAsOut, _readstatus,_setHigh,_setLow, _set,_toggle   };
GPIO_struct SW2_P1_10 =   {1,10, _setAsIn,_setAsOut, _readstatus,_setHigh,_setLow, _set,_toggle   };
GPIO_struct SW3_P1_14 =   {1,14, _setAsIn,_setAsOut, _readstatus,_setHigh,_setLow, _set,_toggle   };
GPIO_struct LED1_P1_0 =   {1,0, _setAsIn,_setAsOut, _readstatus,_setHigh,_setLow, _set,_toggle   };
float set =0.0;

#ifdef CHANGE_MODES_WITH_SWITCH_1
int Modes[4] = {Off,Low,Medium,High};
#endif


void motor_driver(void);

void motor_driver(void)
{
    can_msg_t rx_msg;
    LF_MOTOR_CMD_t msg;
    dbc_msg_hdr_t can_msg_hdr;
    while(CAN_rx(can1,&rx_msg,0))
    {
        LED1_P1_0.set_high(&LED1_P1_0);
        can_msg_hdr.dlc = rx_msg.frame_fields.data_len;
        can_msg_hdr.mid = rx_msg.msg_id;
        dbc_decode_LF_MOTOR_CMD(&msg,rx_msg.data.bytes,&can_msg_hdr);

        if(msg.MOTOR_CMD_steer== steer_front && msg.MOTOR_CMD_drive== drive_stop)
        {
            set = TARGET_STOP_RPM;
            dont_steer();
        }
        else if(msg.MOTOR_CMD_steer==steer_front)
        {
            set= TARGET_LOW_RPM;
            dont_steer();
        }

       else if(msg.MOTOR_CMD_steer==steer_soft_left )
        {
            steer((MAX_REVERSE_DUTY+NEUTRAL_DUTY)/2.0);   // soft left
        }
        else if(msg.MOTOR_CMD_steer==steer_soft_right)
           {
               steer((NEUTRAL_DUTY+MAX_FORWARD_DUTY)/2.0);   // soft right
           }

    }
    if(dbc_handle_mia_LF_MOTOR_CMD(&msg,10))
    {
        LED1_P1_0.set_low(&LED1_P1_0);
    }


}


bool C_period_init(void) {

    //enable_adc_in_p1_30(); //enables input from potentiometer on pin P1.30.
    motor_init();
    dont_move_motor();
    dont_steer();
    SW1_P1_9.set_as_input(&SW1_P1_9);
    SW2_P1_10.set_as_input(&SW2_P1_10);
    SW3_P1_14.set_as_input(&SW3_P1_14);
    LED1_P1_0.set_as_output(&LED1_P1_0);
    CAN_init(can1,100,100,100,NULL,NULL);
    CAN_reset_bus(can1);
    CAN_bypass_filter_accept_all_msgs();
    CAN_reset_bus(can1);
    return true;
}

bool C_period_reg_tlm(void) {
    return true;
}

void C_period_1Hz(uint32_t count) {
    (void) count;

    if(CAN_is_bus_off(can1))
    {
        CAN_reset_bus(can1);
        CAN_bypass_filter_accept_all_msgs();
        CAN_reset_bus(can1);
    }

    LF_MOTOR_CAN_DEBUG_MSG_t status_msg;
        get_motor_rpm_in_mph(NULL);
        c_led_display_set_number((int)MotorSpeedStatus.rpmSpeedInMph);

        //send
        status_msg.car_mph = MotorSpeedStatus.MotorSpeed;
        status_msg.axle_rps = get_rps();
        dbc_encode_and_send_LF_MOTOR_CAN_DEBUG_MSG(&status_msg);

#if 1
        set_motor_rpm_in_mph(TARGET_LOW_RPM,&MotorSpeedStatus.MotorSpeed);
        move_forward_variable(MotorSpeedStatus.MotorSpeed);
#endif
//        set_motor_rpm_in_mph(set,&MotorSpeedStatus.MotorSpeed);
//        move_forward_variable(MotorSpeedStatus.MotorSpeed);

        printf("RPM: %0.2f | PWM: %0.2f \n",\
                MotorSpeedStatus.rpmSpeedInMph,MotorSpeedStatus.MotorSpeed);


}

void C_period_10Hz(uint32_t count) {
    (void) count;

    LF_MOTOR_HB_t HB = {1};
    dbc_encode_and_send_LF_MOTOR_HB(&HB);
    if(SW1_P1_9.readpin(&SW1_P1_9))
    {
        dont_move_motor();
    }
    else
    {
        //motor_driver();
    }



#ifdef CHANGE_MODES_WITH_SWITCH_1
    static int modeIndex=0;
    if(SW1_P1_9.readpin(&SW1_P1_9))
    {
        modeIndex = (modeIndex + 1) % sizeof(Modes);
        move_forward(Modes[modeIndex]);
        move_forward(Off);
        dont_move_motor();

    }
    else if(SW2_P1_10.readpin(&SW2_P1_10))
            {
                 move_forward(Low);
                 //set_point_mph=3;
            }
    else if(SW3_P1_14.readpin(&SW3_P1_14))
            {
                 move_forward(Medium);
                 //set_point_mph=8;
            }
    else if(SW4_P1_15.readpin(&SW4_P1_15))
            {
                 move_forward(High);
                 //set_point_mph=10;
            }


#endif

#ifdef MANUAL

    /* GLOBAL VARIABLES:

    GPIO_struct SW1_P1_9 =   {1,9, _setAsIn,_setAsOut, _readstatus,_setHigh,_setLow, _set,_toggle   };
    GPIO_struct SW2_P1_10 =  {1,10,_setAsIn,_setAsOut, _readstatus,_setHigh,_setLow, _set,_toggle   };
    GPIO_struct SW3_P1_14 =  {1,14,_setAsIn,_setAsOut, _readstatus,_setHigh,_setLow, _set,_toggle   };
    GPIO_struct SW4_P1_15 =  {1,15,_setAsIn,_setAsOut, _readstatus,_setHigh,_setLow, _set,_toggle   };
    int ind =0;
    int i[3] = {Low,Medium,High};

    */

    SW1_P1_9.set_as_input(&SW1_P1_9);
    SW2_P1_10.set_as_input(&SW2_P1_10);
    SW3_P1_14.set_as_input(&SW3_P1_14);
    SW4_P1_15.set_as_input(&SW4_P1_15);

    if(SW2_P1_10.readpin(&SW2_P1_10))
       {
           ind = (ind + 1) % 3;
           printf("Speed: %d\n",ind);
           move_forward(i[ind]);
       }
       else if(SW1_P1_9.readpin(&SW1_P1_9))
       {
           dont_move(Motor);

       }
       else if(SW3_P1_14.readpin(&SW3_P1_14))
       {
           move_backward();
       }

#endif


}

void C_period_100Hz(uint32_t count) {
    (void) count;



}

void C_period_1000Hz(uint32_t count) {
    (void) count;
}
