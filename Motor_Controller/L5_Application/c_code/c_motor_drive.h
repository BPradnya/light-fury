/*
 * c_motor_drive.h
 *
 *  Created on: Apr 2, 2019
 *      Author: gaurav
 */

#ifndef C_MOTOR_DRIVE_H_
#define C_MOTOR_DRIVE_H_

#include <stdbool.h>
#include "c_led_display.h"
#include "c_pwm.h"
#include "c_adc_val.h"
#include "c_gpio.h"
#include "rpm.h"
#include "c_pid.h"

#define NEUTRAL_DUTY        7.5
#define MAX_FORWARD_DUTY    10.0
#define MAX_REVERSE_DUTY    5.0

#define MOTOR_FW_LOW_SPEED_DUTY    8.5
#define MOTOR_FW_MEDIUM_SPEED_DUTY 8.8
#define MOTOR_FW_HIGH_SPEED_DUTY   9.0

#define MOTOR_BW_SPEED_DUTY        6.5

//RPM MACROS
#define WHEEL_CIRCUMFERENCE_CENTIMETERS             33.7
#define NUM_OF_AXLE_ROTATIONS_PER_WHEEL_ROTATION    2.5
#define SCALE_FACTOR_RPM                            1/NUM_OF_AXLE_ROTATIONS_PER_WHEEL_ROTATION
#define DISTANCE_PER_AXLE_ROT                       (WHEEL_CIRCUMFERENCE_CENTIMETERS\
                                                    /NUM_OF_AXLE_ROTATIONS_PER_WHEEL_ROTATION)

//PID MACRO
#define PROPORTIONAL_CONSTANT 0.05

enum{
steer_front = 1,
steer_soft_left,
steer_hard_left,
steer_soft_right,
steer_hard_right
};

enum{
drive_forward = 1,
drive_reverse,
drive_stop
};

typedef enum
{
    Off,
    Low,
    Medium,
    High
}Motor_speed;

typedef struct SpeedStatus
{
     float MotorSpeed;
     float rpmSpeedInMph;
     float MotorDuty_fw;
}SpeedStatus_t;

SpeedStatus_t MotorSpeedStatus;

void Intr_r(void);

bool motor_init(void);

bool steer(float percent);

//bool steer_left(float percent);
//
//bool steer_right(float percent);

bool move_forward(Motor_speed speed);
bool move_forward_variable(float speed);

bool move_backward(void);
bool move_backward_variable(float speed);

bool dont_move_motor(void);
bool dont_steer(void);

int get_rps(void);

//Must be called in loop
void get_motor_rpm_in_mph(float* mph);
void set_motor_rpm_in_mph(float targetMph, float* getDutyCycle);

#endif /* C_MOTOR_DRIVE_H_ */
