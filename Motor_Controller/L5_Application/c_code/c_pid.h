/*
 * c_pid.h
 *
 *  Created on: Apr 18, 2019
 *      Author: Niraj
 */

#ifndef C_PID_H_
#define C_PID_H_

#include <stdbool.h>

typedef struct
{
    float Kp;
    float minOutputRange;
    float maxOutputRange;
}p_t;

bool pid_init( float Kp, float minOutputRange, float maxOutputRange);

void pid_setPoint(  float setPoint,float* currentReading, float* output);

#endif /* C_PID_H_ */
