/*
 * c_pwm.h
 *
 *  Created on: Apr 2, 2019
 *      Author: gaurav
 */

#ifndef C_PWM_H_
#define C_PWM_H_


#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>


typedef enum {
       Motor, ///< P2.1
       Servo, ///< P2.2
}Motor_select;

bool c_pwm_init(Motor_select pwm, unsigned int freq);

bool c_pwm_set(Motor_select pwm,float percent);


#ifdef __cplusplus
}
#endif




#endif /* C_PWM_H_ */
