#include "unity.h"
#include "GPSSensor.h"

#include "Mockc_uart3.h"
#include "Mockc_gpio.h"
#include "MockGeoDecoder.h"

void test_gps_init(void) 
{
    uart3_init_ExpectAndReturn(115200, 118, 1, true);
    TEST_ASSERT_TRUE(gpsUartInit(115200, 118, 1));
}

void test_gpsLedLockInit(void)
{
    setPinPortAsOutput_ExpectAndReturn(1, 0, true);
    TEST_ASSERT_TRUE(setPinPortAsOutput(1, 0));
}

void test_checkGpslock(void)
{	
	if(true)
 	{
		ledOn_ExpectAndReturn(1, true);
		TEST_ASSERT_TRUE(ledOn(1));
	}
	else{
  		ledOff_ExpectAndReturn(1, true);
		TEST_ASSERT_TRUE(ledOff(1));
	}
}

void test_getGpsCompassValue(void)
{
	decode_ExpectAndReturn(16, false);
	TEST_ASSERT_FALSE(decode(16));
	decode_ExpectAndReturn(32, true);
	TEST_ASSERT_TRUE(decode(32));
}
