#include "unity.h"
#include "c_period_callbacks.h"
#include "Mockcan.h"
#include "Mockc_ultrasonic.h"
#include "MockCan_Operation.h"
// #include "_can_dbc/generated_can.h"
#include "Mockutilities.h"  //for delay_us
#include "Mocklpc_sys.h"    // for sys_get_uptime_ms
#include "Mockeint.h"
#include "Mockc_gpio.h"
#include "Mockc_enable_external_intr.h"
#include "Mockc_calc_distance.h"


void setUp(void)
{
    puts("\nsetUp() is called before each test_* function");
}

void tearDown(void)
{
    puts("\ntearDown() is called after each test_* function (unless test is ignored)");
}

void test_C_period_init(void)
{
    CAN_init_ExpectAndReturn(can1, 100, 100, 100, NULL, NULL, true);
    CAN_reset_bus_Expect(can1);
    CAN_bypass_filter_accept_all_msgs_Expect();
    CAN_reset_bus_Expect(can1);
    ultrasonic_init_ExpectAndReturn(true);
    TEST_ASSERT_TRUE(C_period_init());
}

void test_C_period_1Hz(void)
{
    CAN_is_bus_off_ExpectAndReturn(can1, true);
    CAN_reset_bus_Expect(can1);
    CAN_bypass_filter_accept_all_msgs_Expect();
    CAN_reset_bus_Expect(can1);
    C_period_1Hz(0);

    CAN_is_bus_off_ExpectAndReturn(can1, false);
    C_period_1Hz(0);
}

void test_C_period_10Hz(void)
{
    dbc_app_send_can_msg_ExpectAnyArgsAndReturn(true);
    C_period_10Hz(0);
}

GPIO_struct left_sen_P2_1 = { 2, 1, _setAsIn, _setAsOut, _readstatus, _setHigh, _setLow, _set, _toggle };
GPIO_struct front_sen_P2_2 = { 2, 2, _setAsIn, _setAsOut, _readstatus, _setHigh, _setLow, _set, _toggle };
GPIO_struct right_sen_P2_3 = { 2, 3, _setAsIn, _setAsOut, _readstatus, _setHigh, _setLow, _set, _toggle };
GPIO_struct rear_sen_P2_4 = { 2, 4, _setAsIn, _setAsOut, _readstatus, _setHigh, _setLow, _set, _toggle };


void test_C_period_100Hz(void)
{
    
   static uint8_t calculated_distance[4] = {0, 0, 0, 0};
//    uint8_t* var =
    trigger_ultrasonic_sensor_ExpectAndReturn(&front_sen_P2_2, true);
    trigger_ultrasonic_sensor_ExpectAndReturn(&rear_sen_P2_4, true);
    C_period_100Hz(6);

    trigger_ultrasonic_sensor_ExpectAndReturn(&left_sen_P2_1, true);
    trigger_ultrasonic_sensor_ExpectAndReturn(&right_sen_P2_3, true);
    C_period_100Hz(8);


    calculate_distance_inch_ExpectAndReturn(calculated_distance);
    Can_tx_ExpectAndReturn(calculated_distance, true);
    C_period_100Hz(10);
}

void test_C_period_1000Hz(void){
    C_period_1000Hz(1);
}

