#include "unity.h"
#include "c_ultrasonic.h"
// #include "Mockc_gpio.h"
#include "Mockutilities.h"  //for delay_us
#include "Mocklpc_sys.h"    // for sys_get_uptime_ms
#include "Mockeint.h"
#include "Mockc_gpio.h"
#include "Mockc_enable_external_intr.h"
#include "Mockc_calc_distance.h"


void setUp(void) {

}

void tearDown(void) {

}

void test_ultrasonic_init(void)
{
    enable_external_interrupt_ExpectAndReturn(2,true);
    enable_external_interrupt_ExpectAndReturn(4,true);
    enable_external_interrupt_ExpectAndReturn(1,true);
    enable_external_interrupt_ExpectAndReturn(3,true);
    TEST_ASSERT_TRUE(ultrasonic_init());
}


void test_trigger_ultrasonic_sensor(void)
{
    GPIO_struct left_sensor_P2_1 = { 2, 1, _setAsIn, _setAsOut, _readstatus, _setHigh, _setLow, _set, _toggle };

    _setAsOut_Expect(&left_sensor_P2_1);
    _setLow_Expect(&left_sensor_P2_1);
    delay_us_Expect(LOW_PULSE_DURATION);
    _setHigh_Expect(&left_sensor_P2_1);
    delay_us_Expect(HIGH_PULSE_DURATION);
    _setLow_Expect(&left_sensor_P2_1);
    _setAsIn_Expect(&left_sensor_P2_1);
    TEST_ASSERT_TRUE(trigger_ultrasonic_sensor(&left_sensor_P2_1));
}
