#include "unity.h"
#include "BlueTooth.h"

#include "Mockc_uart.h"

static char writeQue[100];

void test_initUart(void) 
{
	uart2_init_ExpectAndReturn(9600,100,true);
	uart3_init_ExpectAndReturn(9600,100,true);	
	TEST_ASSERT_TRUE(initUart());

	uart2_init_ExpectAndReturn(9600,100,true);
	uart3_init_ExpectAndReturn(9600,100,false);	
	TEST_ASSERT_FALSE(initUart());
}

void test_BT_receive(void) 
{
    uart2_gets_ExpectAndReturn(writeQue,sizeof(writeQue),0,true);
    TEST_ASSERT_TRUE(BT_receive(writeQue,sizeof(writeQue),0));

    uart2_gets_ExpectAndReturn(writeQue,sizeof(writeQue),0,false);
    TEST_ASSERT_FALSE(BT_receive(writeQue,sizeof(writeQue),0));

}

void test_xBee_receive(void) 
{
    uart3_gets_ExpectAndReturn(writeQue,sizeof(writeQue),0,true);
    TEST_ASSERT_TRUE(xBee_receive(writeQue,sizeof(writeQue),0));

    uart3_gets_ExpectAndReturn(writeQue,sizeof(writeQue),0,false);
    TEST_ASSERT_FALSE(xBee_receive(writeQue,sizeof(writeQue),0));

  
}

void test_BT_transmit(void) 
{
	uart2_putline_Expect(writeQue,0);
	BT_transmit(writeQue,0);
}

void test_xBee_transmit(void) 
{
  	uart3_putline_Expect(writeQue,0);
	xBee_transmit(writeQue,0);
}