#include "unity.h" // Single Unity Test Framework include

#include "c_motor_drive.h"
#include "Mockc_pwm.h"
#include "Mockc_adc_val.h"

void setUp(void) {
}

void tearDown(void) {
}

void test_motor_init(void) {
    
    c_pwm_init_ExpectAndReturn(Motor,50,true);
    c_pwm_init_ExpectAndReturn(Servo,50,true);
    TEST_ASSERT_TRUE(motor_init());

    c_pwm_init_ExpectAndReturn(Motor,50,true);
    c_pwm_init_ExpectAndReturn(Servo,50,false);
    TEST_ASSERT_FALSE(motor_init());
}

void test_servo_steer(void)
{   
    
    c_pwm_set_ExpectAndReturn(Servo,5,true);
    TEST_ASSERT_TRUE(steer(1.0));
    c_pwm_set_ExpectAndReturn(Servo,10,true);
    TEST_ASSERT_TRUE(steer(100.0));

    c_pwm_set_ExpectAndReturn(Servo,5,false);
    TEST_ASSERT_FALSE(steer(1.0));

}

void test_move_forward(void)
{
    Motor_speed obj = Off;
    c_pwm_set_ExpectAndReturn(Motor,NEUTRAL_DUTY,true);
    TEST_ASSERT_TRUE(move_forward(obj));

    obj = Low;
    c_pwm_set_ExpectAndReturn(Motor,MOTOR_FW_LOW_SPEED_DUTY,true);
    TEST_ASSERT_TRUE(move_forward(obj));

    obj = Medium;
    c_pwm_set_ExpectAndReturn(Motor,MOTOR_FW_MEDIUM_SPEED_DUTY,true);
    TEST_ASSERT_TRUE(move_forward(obj));

    obj = High;
    c_pwm_set_ExpectAndReturn(Motor,MOTOR_FW_HIGH_SPEED_DUTY,true);
    TEST_ASSERT_TRUE(move_forward(obj));

    obj = 10;
    c_pwm_set_ExpectAndReturn(Motor,NEUTRAL_DUTY,true);
    TEST_ASSERT_TRUE(move_forward(obj));

    obj = -1;
    c_pwm_set_ExpectAndReturn(Motor,NEUTRAL_DUTY,false);
    TEST_ASSERT_FALSE(move_forward(obj));
}

void test_move_forward_variable(void)
{
    c_pwm_set_ExpectAndReturn(Motor,NEUTRAL_DUTY,true);
    TEST_ASSERT_TRUE(move_forward_variable(1.0));
    c_pwm_set_ExpectAndReturn(Motor,MAX_FORWARD_DUTY,true);
    TEST_ASSERT_TRUE(move_forward_variable(100.0));

    c_pwm_set_ExpectAndReturn(Motor,NEUTRAL_DUTY,false);
    TEST_ASSERT_FALSE(move_forward_variable(1.0));
}

void test_move_backward(void)
{
    c_pwm_set_ExpectAndReturn(Motor,MAX_REVERSE_DUTY,true);
    TEST_ASSERT_TRUE(move_backward());   
}

void test_move_back_variable(void)
{
    c_pwm_set_ExpectAndReturn(Motor,NEUTRAL_DUTY,true);
    TEST_ASSERT_TRUE(move_backward_variable(10.0));
    c_pwm_set_ExpectAndReturn(Motor,MAX_REVERSE_DUTY,true);
    TEST_ASSERT_TRUE(move_backward_variable(1.0));

    c_pwm_set_ExpectAndReturn(Motor,NEUTRAL_DUTY,false);
    TEST_ASSERT_FALSE(move_backward_variable(100.0));
}

void test_dont_move(void)
{
    c_pwm_set_ExpectAndReturn(Motor,NEUTRAL_DUTY,true);
    TEST_ASSERT_TRUE(dont_move(Motor));
    c_pwm_set_ExpectAndReturn(Servo,NEUTRAL_DUTY,true);
    TEST_ASSERT_TRUE(dont_move(Servo));

    c_pwm_set_ExpectAndReturn(Servo,NEUTRAL_DUTY,false);
    TEST_ASSERT_FALSE(dont_move(Servo));

}
