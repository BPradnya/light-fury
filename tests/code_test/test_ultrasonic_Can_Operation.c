#include "unity.h"
#include "Can_Operation.h"
#include "Mockcan.h"

void setUp(void)
{
    puts("\nsetUp() is called before each test_* function");
}

void tearDown(void)
{
    puts("\ntearDown() is called after each test_* function (unless test is ignored)");
}

uint8_t calculated_distance[4];
LF_SENSOR_DATA_t sensor_data = {0};
can_msg_t can_msg = {0};

void test_Can_tx(void)
{
	sensor_data.left_sensor = calculated_distance[0];
    sensor_data.front_sensor = calculated_distance[1];
    sensor_data.right_sensor = calculated_distance[2];
    sensor_data.rear_sensor = calculated_distance[3];
	
	// CAN_tx_ExpectAndReturn(can1, &can_msg, 0, true);
	CAN_tx_ExpectAnyArgsAndReturn(true);
	TEST_ASSERT_TRUE(Can_tx(calculated_distance));
	// TEST_ASSERT_TRUE(dbc_encode_and_send_LF_SENSOR_DATA(&sensor_data));
}