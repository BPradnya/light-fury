/*
 * c_enable_external_intr.c
 *
 *  Created on: May 5, 2019
 *      Author: prita
 */
#include"c_enable_external_intr.h"

volatile uint64_t start_timer_left = 0;
volatile uint64_t start_timer_front = 0;
volatile uint64_t start_timer_right = 0;
volatile uint64_t start_timer_rear = 0;

volatile uint64_t stop_timer_left = 0;
volatile uint64_t stop_timer_front = 0;
volatile uint64_t stop_timer_right = 0;
volatile uint64_t stop_timer_rear = 0;

void start_timer_left_sensor(void)
{
    start_timer_left = sys_get_uptime_us();
}

void stop_timer_and_calc_dist_left_sensor()
{
    stop_timer_left = sys_get_uptime_us();
}

void start_timer_front_sensor(void)
{
    start_timer_front = sys_get_uptime_us();
}

void stop_timer_and_calc_dist_front_sensor(void)
{
    stop_timer_front = sys_get_uptime_us();
}

void start_timer_right_sensor(void)
{
    start_timer_right = sys_get_uptime_us();
}

void stop_timer_and_calc_dist_right_sensor(void)
{
    stop_timer_right = sys_get_uptime_us();
}

void start_timer_rear_sensor(void)
{
    start_timer_rear = sys_get_uptime_us();
}

void stop_timer_and_calc_dist_rear_sensor(void)
{
    stop_timer_rear = sys_get_uptime_us();
}

bool enable_external_interrupt(uint8_t pin_num)
{
    bool status = false;
    switch (pin_num) {
        case 1:
            eint3_enable_port2(pin_num, eint_rising_edge, start_timer_left_sensor);
            eint3_enable_port2(pin_num, eint_falling_edge, stop_timer_and_calc_dist_left_sensor);
            status = true;
            break;

        case 2:
            eint3_enable_port2(pin_num, eint_rising_edge, start_timer_front_sensor);
            eint3_enable_port2(pin_num, eint_falling_edge, stop_timer_and_calc_dist_front_sensor);
            status = true;
            break;

        case 3:
            eint3_enable_port2(pin_num, eint_rising_edge, start_timer_right_sensor);
            eint3_enable_port2(pin_num, eint_falling_edge, stop_timer_and_calc_dist_right_sensor);
            status = true;
            break;

        case 4:
            eint3_enable_port2(pin_num, eint_rising_edge, start_timer_rear_sensor);
            eint3_enable_port2(pin_num, eint_falling_edge, stop_timer_and_calc_dist_rear_sensor);
            status = true;
            break;

        default:
//            printf("Pin num is invalid\n");
            break;
    }
    return status;
}
