/*
 * c_gpio.h
 *
 *  Created on: Mar 5, 2019
 *      Author: gaura
 */


#ifndef C_GPIO_H_
#define C_GPIO_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct GPIO_struct
{
    uint8_t port;
    uint8_t pin;
    void (*set_as_input)(const struct GPIO_struct*);
    void (*set_as_output)(const struct GPIO_struct*);
    bool (*readpin)(const struct GPIO_struct*);
    void (*set_high)(const struct GPIO_struct*);
    void (*set_low)(const struct GPIO_struct*);
    void (*setb)(const struct GPIO_struct*,bool on);
    void (*toggle)(const struct GPIO_struct*);
}GPIO_struct;


void c_gpio_SetAsInput(const struct GPIO_struct* ptr);
void c_gpio_SetAsOutput(const struct GPIO_struct* ptr);

bool c_gpio_read(const struct GPIO_struct* ptr);
void c_gpio_setHigh(const struct GPIO_struct *ptr);
void c_gpio_setLow(const struct GPIO_struct *ptr);
void c_gpio_set(const struct GPIO_struct *ptr, bool on);
void c_gpio_toggle(const struct GPIO_struct *ptr);

/*
 * Wrappers to call in applications during struct initialization
 * Usage Example:
 *  GPIO_struct P2_1 =  {2,1,_setAsIn,_setAsOut,
 *                          _readstatus,_setHigh,_setLow,
 *                          _set,_toggle
 *                      };
 */
void _setAsIn(const struct GPIO_struct* ptr);
void _setAsOut(const struct GPIO_struct* ptr);
bool _readstatus(const struct GPIO_struct* ptr);
void _setHigh(const struct GPIO_struct* ptr);
void _setLow(const struct GPIO_struct* ptr);
void _set(const struct GPIO_struct* ptr,bool on);
void _toggle(const struct GPIO_struct* ptr);


#ifdef __cplusplus
}
#endif

#endif /* C_GPIO_H_ */
