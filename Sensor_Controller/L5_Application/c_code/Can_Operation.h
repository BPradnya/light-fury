/*
 * Can_tx.h
 *
 *  Created on: Mar 5, 2019
 *      Author: kaust
 */

#ifndef CAN_OPERATION_H_
#define CAN_OPERATION_H_

#include <string.h>
#include <stdio.h>
#include "can.h"
#include "_can_dbc/generated_can.h"

bool Can_tx(uint8_t* calculated_distance);
bool dbc_app_send_can_msg(uint32_t mid, uint8_t dlc, uint8_t bytes[8]);

#endif /* CAN_OPERATION_H_ */
