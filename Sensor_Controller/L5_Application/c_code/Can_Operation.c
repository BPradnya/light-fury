/*
 * Can_tx.c
 *
 *  Created on: Mar 5, 2019
 *      Author: kaust
 */


#include "Can_Operation.h"
//#include "unit_test_facilitator.h"

bool dbc_app_send_can_msg(uint32_t mid, uint8_t dlc, uint8_t bytes[8])
{
    can_msg_t can_msg = { 0 };
    can_msg.msg_id                = mid;
    can_msg.frame_fields.data_len = dlc;
    memcpy(can_msg.data.bytes, bytes, dlc);
//    printf("sending msg: %d, %d, %d, %d\n", can_msg.data.bytes[0], can_msg.data.bytes[1], can_msg.data.bytes[2], can_msg.data.bytes[3]);
    return CAN_tx(can1, &can_msg, 0);
}

bool Can_tx(uint8_t* calculated_distance)
{
    LF_SENSOR_DATA_t sensor_data = {0};

    sensor_data.left_sensor = calculated_distance[0];
    sensor_data.front_sensor = calculated_distance[1];
    sensor_data.right_sensor = calculated_distance[2];
    sensor_data.rear_sensor = calculated_distance[3];

    return dbc_encode_and_send_LF_SENSOR_DATA(&sensor_data);
}
