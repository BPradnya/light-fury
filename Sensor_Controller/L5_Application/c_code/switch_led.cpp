/*
 * switch_led.cpp
 *
 *  Created on: Mar 3, 2019
 *      Author: kaust
 */

#include "switch_led.h"
#include "io.hpp"
#include "printf_lib.h"

bool readSwitch(int pinNum)
{
    return SW.getSwitch(pinNum);
}

void ledOn(int pinNum)
{
    printf("turn on LED\n");
    LE.on(pinNum);
}

void ledOff(int pinNum)
{
    printf("turn off LED\n");
    LE.off(pinNum);
}

