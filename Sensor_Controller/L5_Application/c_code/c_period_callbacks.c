/**
 * @file
 *
 * The purpose of this "C" callbacks is to provide the code to be able
 * to call pure C functions and unit-test it in C test framework
 */

#include "c_period_callbacks.h"

#define LIGHTFURY 1 //Autonomous car

GPIO_struct left_sensor_P2_1 = { 2, 1, _setAsIn, _setAsOut, _readstatus, _setHigh, _setLow, _set, _toggle };
GPIO_struct front_sensor_P2_2 = { 2, 2, _setAsIn, _setAsOut, _readstatus, _setHigh, _setLow, _set, _toggle };
GPIO_struct right_sensor_P2_3 = { 2, 3, _setAsIn, _setAsOut, _readstatus, _setHigh, _setLow, _set, _toggle };
GPIO_struct rear_sensor_P2_4 = { 2, 4, _setAsIn, _setAsOut, _readstatus, _setHigh, _setLow, _set, _toggle };

bool C_period_init(void) {
#if LIGHTFURY
    CAN_init(can1,100,100,100,NULL,NULL);
    CAN_reset_bus(can1);
    CAN_bypass_filter_accept_all_msgs();
    CAN_reset_bus(can1);
    ultrasonic_init();
#endif

    return true;
}

bool C_period_reg_tlm(void) {
    return true;
}

void C_period_1Hz(uint32_t count) {
    (void) count;
    if(CAN_is_bus_off(can1))
    {
        CAN_reset_bus(can1);
        CAN_bypass_filter_accept_all_msgs();
        CAN_reset_bus(can1);
    }
}



void C_period_10Hz(uint32_t count) {
    (void) count;
#if LIGHTFURY
    LF_SENSOR_HB_t sensor_hb = {1};
    dbc_encode_and_send_LF_SENSOR_HB(&sensor_hb);
#endif
}

void C_period_100Hz(uint32_t count) {
    (void) count;
#if LIGHTFURY

    if(count % 5 == 1){
        trigger_ultrasonic_sensor(&front_sensor_P2_2);
        trigger_ultrasonic_sensor(&rear_sensor_P2_4);
    }

    if(count % 5 == 3){
        trigger_ultrasonic_sensor(&left_sensor_P2_1);
        trigger_ultrasonic_sensor(&right_sensor_P2_3);
    }

    if(count % 5 == 0)
    {
        uint8_t* calculated_distance = calculate_distance_inch();
        Can_tx(calculated_distance);
    }

#endif
}


void C_period_1000Hz(uint32_t count) {
    (void) count;
}
