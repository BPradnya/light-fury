/*
 * lab_queue.h
 *
 *  Created on: Mar 1, 2019
 *      Author: prita
 */

#ifndef LAB_QUEUE_H_
#define LAB_QUEUE_H_

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

typedef struct {
  uint8_t queue_memory[100];
  int front;
  int rear;
  uint8_t len;
  uint8_t elements;
  // TODO: Add more members as needed
} queue_S;

void queue__init(queue_S *queue);

bool queue__push(queue_S *queue, uint8_t push_value);

bool queue__pop(queue_S *queue, uint8_t *pop_value);

size_t queue__get_count(queue_S *queue);


#endif /* LAB_QUEUE_H_ */
