/*
 * c_enable_external_intr.h
 *
 *  Created on: May 5, 2019
 *      Author: prita
 */

#ifndef C_ENABLE_EXTERNAL_INTR_H_
#define C_ENABLE_EXTERNAL_INTR_H_

#include<stdbool.h>
#include "eint.h"

bool enable_external_interrupt(uint8_t pin_num);

#endif /* C_ENABLE_EXTERNAL_INTR_H_ */
