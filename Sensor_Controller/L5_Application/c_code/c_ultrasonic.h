/*
 * c_ultrasonic.h
 *
 *  Created on: Mar 30, 2019
 *      Author: prita
 */

#ifndef C_ULTRASONIC_H_
#define C_ULTRASONIC_H_
#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "utilities.h"
#include "c_enable_external_intr.h"
#include "c_calc_distance.h"
#include "c_gpio.h"

#define HIGH_PULSE_DURATION 5
#define LOW_PULSE_DURATION 2

typedef enum{
    left,
    front,
    right,
    rear
}SENSOR_POS;

bool ultrasonic_init(void);
bool trigger_ultrasonic_sensor(const struct GPIO_struct* ptr);


#ifdef __cplusplus
}
#endif
#endif /* C_ULTRASONIC_H_ */
