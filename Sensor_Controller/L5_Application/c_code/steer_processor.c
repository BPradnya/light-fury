/*
 * steer_processor.c
 *
 *  Created on: Feb 24, 2019
 *      Author: prita
 */

#include <stdio.h>
#include <stdint.h>
#include "steer_processor.h"

void steer_processor(uint32_t left_sensor_cm, uint32_t right_sensor_cm){
    if((left_sensor_cm > right_sensor_cm) && (right_sensor_cm < 50)){
        steer_left();
    }
    else if((right_sensor_cm > left_sensor_cm) && (left_sensor_cm < 50)){
        steer_right();
    }
    else{}
}
