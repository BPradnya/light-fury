/*
 * lab_queue.c
 *
 *  Created on: Mar 1, 2019
 *      Author: prita
 */

#include "lab_queue.h"
#include <stdio.h>

void queue__init(queue_S *queue){
    queue->front = -1;
    queue->rear = -1;
    queue->len = 100;
    queue->elements = 0;
}

bool queue__push(queue_S *queue, uint8_t push_value){
    if((queue->front == -1) && (queue->rear == -1))
        queue->front = queue->rear = 0;
    else if(((queue->rear+1) % queue->len) == queue->front)
        return false;
    else
        queue->rear = (queue->rear+1) % queue->len;

    queue->queue_memory[queue->rear] = push_value;
    queue->elements++;
//    printf("F: %d R: %d\n",queue->front, queue->rear);
    return true;
}

bool queue__pop(queue_S *queue, uint8_t* pop_value){
    if((queue->front == -1) && (queue->rear == -1))
        return false;
    else if((queue->front % queue->len) == (queue->rear % queue->len)){
        *pop_value = queue->queue_memory[queue->front];
        queue->queue_memory[queue->front] = 0;
        queue->front = queue->rear = -1;
        queue->elements--;
        return true;
    }

    *pop_value = queue->queue_memory[queue->front];
    queue->queue_memory[queue->front] = 0;
    queue->front = (queue->front+1) % queue->len;
    queue->elements--;
//    printf("F: %d R: %d\n",queue->front, queue->rear);
    return true;
}

size_t queue__get_count(queue_S *queue){
    return queue->elements;
}
