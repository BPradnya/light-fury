/*
 * steering.h
 *
 *  Created on: Feb 23, 2019
 *      Author: prita
 */

#ifndef STEERING_H_
#define STEERING_H_

#pragma once

void steer_left(void);
void steer_right(void);



#endif /* STEERING_H_ */
