/*
 * c_calc_distance.h
 *
 *  Created on: May 5, 2019
 *      Author: prita
 */

#ifndef C_CALC_DISTANCE_H_
#define C_CALC_DISTANCE_H_

#include <stdint.h>

#define DISTANCE_SCALE_FACTOR   0.0175
#define DISTANCE_SCALE_FACTOR_INCHES   147

uint8_t* calculate_distance_inch(void);

#endif /* C_CALC_DISTANCE_H_ */
