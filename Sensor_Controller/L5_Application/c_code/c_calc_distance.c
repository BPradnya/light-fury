/*
 * c_calc_distance.c
 *
 *  Created on: May 5, 2019
 *      Author: prita
 */

#include "c_calc_distance.h"

static uint8_t calculated_distance[4];

extern uint64_t start_timer_left;
extern uint64_t start_timer_front;
extern uint64_t start_timer_right;
extern uint64_t start_timer_rear;

extern uint64_t stop_timer_left;
extern uint64_t stop_timer_front;
extern uint64_t stop_timer_right;
extern uint64_t stop_timer_rear;

uint8_t* calculate_distance_inch(void)
{
    calculated_distance[0] = (stop_timer_left - start_timer_left) / DISTANCE_SCALE_FACTOR_INCHES;
    calculated_distance[1] = (stop_timer_front - start_timer_front) / DISTANCE_SCALE_FACTOR_INCHES;
    calculated_distance[2] = (stop_timer_right - start_timer_right) / DISTANCE_SCALE_FACTOR_INCHES;
    calculated_distance[3] = (stop_timer_rear - start_timer_rear) / DISTANCE_SCALE_FACTOR_INCHES;
    return calculated_distance;
}
