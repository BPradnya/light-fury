/*
 * c_ultrasonic.c
 *
 *  Created on: Apr 16, 2019
 *      Author: Niraj
 */

#include "c_ultrasonic.h"

//1. initial phase - calculating echo return pulse using
//   while loop (Not accurate method)
//2. Using external interrupts (edge detection)

bool ultrasonic_init(void)
{
    bool status = false;
    if ((enable_external_interrupt(2)) && (enable_external_interrupt(4)) &&\
        (enable_external_interrupt(1)) && (enable_external_interrupt(3))
       )
       {
            status = true;
       }
    return status;
}

bool trigger_ultrasonic_sensor(const struct GPIO_struct* ptr)
{
    bool status = false;
    ptr->set_as_output(ptr);
    ptr->set_low(ptr);
    delay_us(LOW_PULSE_DURATION);
    ptr->set_high(ptr);
    delay_us(HIGH_PULSE_DURATION);
    ptr->set_low(ptr);
    ptr->set_as_input(ptr);
    status = true;

    return status;
}




