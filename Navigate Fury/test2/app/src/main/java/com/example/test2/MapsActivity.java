package com.example.test2;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    Map<String, String> input_map = new HashMap<String, String>();
    private Marker mVisitingMarker;
    private LatLng dest_loc;
    private static DecimalFormat df2 = new DecimalFormat("#.######");
    Button setDestination;
    String address = "98:D3:32:10:D8:41";
    BluetoothAdapter myBluetooth = null;
    BluetoothSocket btSocket = null;
    private boolean isBtConnected = false;
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private ConnectedThread mConnectedThread;
    private static final String TAG = "MY_APP_DEBUG_TAG";
    private Handler handler; // handler that gets info from Bluetooth service

    final int handlerState = 0;                //used to identify handler message

    String readMessage = new String();
    LatLng location_latng;

    @SuppressLint("HandlerLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        setDestination = findViewById(R.id.btnSetDestination);


        handler = new Handler() {
            public void handleMessage(android.os.Message msg) {

                byte[] writeBuf = (byte[]) msg.obj;
                int begin = msg.arg1;
                int end = msg.arg2;

                if (msg.what == handlerState) {                   //if message is what we want
                    readMessage = new String(writeBuf);
                    readMessage = readMessage.substring(begin, end);// msg.arg1 = bytes from connect thread
                    Log.println(Log.INFO, "message", readMessage);
                    String[] inp = readMessage.split(",", 0);
                    if (inp.length == 9) {
                        input_map.put("heading", inp[8]);
                        input_map.put("latitude", inp[6]);
                        input_map.put("longitude", inp[7]);
                        input_map.put("left_sensor", inp[0]);
                        input_map.put("front_sensor", inp[1]);
                        input_map.put("right_sensor", inp[2]);
                        input_map.put("rear_sensor", inp[3]);
                        input_map.put("target_speed", inp[4]);
                        input_map.put("current_speed", inp[5]);
//                        if (Integer.parseInt(input_map.get("heading")) !=0)
//                        {
//                            heading.append(input_map.get("heading") + "\n");
//                        }
//                        if (Integer.parseInt(input_map.get("latitude")) != 0)
//                        {
//                            location.append(input_map.get("latitude") +", " +  input_map.get("longitude") + "\n");
//                        }

                        double latitude = Double.parseDouble(input_map.get("latitude"));
                        double longitude = Double.parseDouble(input_map.get("longitude"));
                        location_latng = new LatLng(latitude,longitude);

                    }

//                  lumn.append(readMessage);

                }
            }
        };

        connectBt();
        setDestination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            String loc = null;
                if (dest_loc!=null) {
                    loc = df2.format(dest_loc.latitude)+ ", " + df2.format((dest_loc.longitude) );
                    Log.println(Log.INFO, "Location sent", loc);
                }
                if (loc!=null)
                    sendSignal(loc);
            }
        });
    }

    @Override
    public void onResume() {

        if (!btSocket.isConnected()) {
            Log.println(Log.INFO, "Resume", "LED");
            connectBt();
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            //Don't leave Bluetooth sockets open when leaving activity
            btSocket.close();
        } catch (IOException e2) {
            //insert code to deal with this
        }
    }

    private void connectBt() {
        //create device and set the MAC address
        myBluetooth = BluetoothAdapter.getDefaultAdapter();
        BluetoothDevice device = myBluetooth.getRemoteDevice(address);

        if (device == null) {
            Log.println(Log.ERROR, "fury", "Null bt device");
        }

        try {
            btSocket = createBluetoothSocket(device);
        } catch (IOException e) {
            Toast.makeText(getBaseContext(), "Socket creation failed", Toast.LENGTH_LONG).show();
            Log.println(Log.ERROR, "fury", "Socket failed");
        }

        // Establish the Bluetooth socket connection.
        try {
            btSocket.connect();
            Toast.makeText(getBaseContext(), "Connected", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            try {
                e.printStackTrace();
                btSocket.close();
            } catch (IOException e2) {
                //insert code to deal with this
            }
        }
        mConnectedThread = new ConnectedThread(btSocket);
        mConnectedThread.start();
    }

    private void sendSignal(String number) {
        if (mConnectedThread != null) {
            number = number + "\n";
            mConnectedThread.write(number);
        }
    }

    private void msg(String s) {
        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                // Creating a marker
                MarkerOptions markerOptions = new MarkerOptions();

                // Setting the position for the marker
                markerOptions.position(latLng);

                // Setting the title for the marker.
                // This will be displayed on taping the marker

                markerOptions.title(latLng.latitude + " : " + latLng.longitude);
                // Clears the previously touched position
                mMap.clear();
                // Animating to the touched position
                mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                // Placing a marker on the touched position
                mMap.addMarker(markerOptions);
                dest_loc = latLng;

            }
        });
        // Add a marker in Sydney and move the camera
        LatLng sanJose = new LatLng(37.335341, -121.881061);
        CameraPosition sj = CameraPosition.builder().target(sanJose).bearing(90).tilt(45).zoom(17).build();
        mMap.addMarker(new MarkerOptions().position(sanJose).title("SJSU"));
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(sj));
    }

    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {

        return device.createRfcommSocketToServiceRecord(myUUID);
        //creates secure outgoing connection with BT device using UUID
    }

    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        //creation of the connect thread
        public ConnectedThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                //Create I/O streams for connection
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }


        public void run() {
            byte[] buffer = new byte[1024];
            int begin = 0;
            int bytes = 0;
            while (true) {
                try {
                    bytes += mmInStream.read(buffer, bytes, buffer.length - bytes);
                    for (int i = begin; i < bytes; i++) {
//                        Log.println(Log.INFO, "bytes",String.valueOf(bytes));
                        if (buffer[i] == "#".getBytes()[0]) {
//                            Log.println(Log.INFO, "i = ", String.valueOf(i));
                            handler.obtainMessage(handlerState, begin, i, buffer).sendToTarget();
                            begin = 0;
                            bytes = 0;
                        }
                    }
                } catch (IOException e) {
                    break;
                }
            }
        }

        //write method
        public void write(String input) {
            byte[] msgBuffer = input.getBytes();           //converts entered String into bytes
            try {
                mmOutStream.write(msgBuffer);                //write bytes over
                // BT connection via outstream
            } catch (IOException e) {
                //if you cannot write, close the application
                Toast.makeText(getBaseContext(), "Connection Failure", Toast.LENGTH_LONG).show();
                finish();

            }
        }
    }
}
