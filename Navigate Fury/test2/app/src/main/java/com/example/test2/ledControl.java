package com.example.test2;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.AsyncTask;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ledControl extends AppCompatActivity {

    ImageButton btnStart;
    Button btnStop, btnSend;
    EditText edt_get_text;
    String address = "98:D3:32:10:D8:41";
    //    String address = null;
    TextView heading, location, sensors, actual_speed, target_speed,checkpoint_num, checkpoint_lat, checkpoint_long, distance, turn_angle, gps_lock;
    Map<String, String> input_map = new HashMap<String, String>();

    private ProgressDialog progress;
    BluetoothAdapter myBluetooth = null;
    BluetoothSocket btSocket = null;
    private boolean isBtConnected = false;
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private ConnectedThread mConnectedThread;
    private static final String TAG = "MY_APP_DEBUG_TAG";
    private Handler handler; // handler that gets info from Bluetooth service

    final int handlerState = 0;                //used to identify handler message

    String readMessage = new String();

    @SuppressLint("HandlerLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        System.out.println("Inside led Control on create");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_led_control);
        btnStart = findViewById(R.id.start);
        btnStop = findViewById(R.id.stop);
        heading = findViewById(R.id.tv_head);

        location = findViewById(R.id.tv_coordinate);
        sensors = findViewById(R.id.textsensor);
        actual_speed = findViewById(R.id.textSpeed);
        target_speed = findViewById(R.id.textDesiredSpeed);
        location.setMovementMethod(new ScrollingMovementMethod());
        heading.setMovementMethod(new ScrollingMovementMethod());
        checkpoint_num = findViewById(R.id.tv_checkpoint_num);
        checkpoint_lat = findViewById(R.id.tv_checkpoint_lat);
        checkpoint_long =findViewById(R.id.tv_checkpoint_long);
        turn_angle = findViewById(R.id.tv_turn_angle);
        distance = findViewById(R.id.tv_distance);
        gps_lock = findViewById(R.id.tv_gps_lock);


        handler = new Handler() {
            public void handleMessage(android.os.Message msg) {

                byte[] writeBuf = (byte[]) msg.obj;
                int begin = msg.arg1;
                int end = msg.arg2;

                if (msg.what == handlerState) {                   //if message is what we want
                    readMessage = new String(writeBuf);
                    readMessage = readMessage.substring(begin, end);// msg.arg1 = bytes from connect thread
                    Log.println(Log.INFO, "message", readMessage);
                    String[] inp = readMessage.split(",", 0);
                    if (inp.length == 15) {
                        input_map.put("heading", inp[8]);
                        input_map.put("latitude", inp[6]);
                        input_map.put("longitude", inp[7]);
                        input_map.put("left_sensor", inp[0]);
                        input_map.put("front_sensor", inp[1]);
                        input_map.put("right_sensor", inp[2]);
                        input_map.put("rear_sensor", inp[3]);
                        input_map.put("target_speed", inp[4]);
                        input_map.put("gps_lock", inp[5]);
                        input_map.put("turning_angle_debug", inp[10]);
                        input_map.put("current_checkpoint", inp[11]);
                        input_map.put("current_radius", inp[12]);
                        input_map.put("checkpoint_lat", inp[13]);
                        input_map.put("checkpoint_long", inp[14]);


//                        if (Integer.parseInt(input_map.get("heading")) !=0)
//                        {
//                            heading.append(input_map.get("heading") + "\n");
//                        }
//                        if (Integer.parseInt(input_map.get("latitude")) != 0)
//                        {
//                            location.append(input_map.get("latitude") +", " +  input_map.get("longitude") + "\n");
//                        }
                        heading.setText(input_map.get("heading"));
                        location.setText(input_map.get("latitude") + ", " + input_map.get("longitude"));
                        sensors.setText((input_map.get("left_sensor") + " , " + input_map.get("front_sensor") + " , " + input_map.get("right_sensor")).replace("\n", ""));
                        actual_speed.setText(input_map.get("current_speed"));
                        target_speed.setText(input_map.get("target_speed"));
                        checkpoint_num.setText(input_map.get("current_checkpoint"));
                        checkpoint_lat.setText(input_map.get("checkpoint_lat"));
                        checkpoint_long.setText(input_map.get("checkpoint_long"));
                        turn_angle.setText(input_map.get("turning_angle_debug"));
                        distance.setText(input_map.get("current_radius"));
                        gps_lock.setText(input_map.get("gps_lock"));


                    }

//                  lumn.append(readMessage);

                }
            }
        };

        connectBt();
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.println(Log.INFO, "Start", "Start button pressed");
                String num = new String();
                num += "2" + "\n";
                sendSignal(num);
            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.println(Log.INFO, "Stop", "Stop button pressed");
                String num = new String();
                num += "1" + "\n";
                sendSignal(num);
            }
        });
    }

    private void connectBt() {
        //create device and set the MAC address
        myBluetooth = BluetoothAdapter.getDefaultAdapter();
        BluetoothDevice device = myBluetooth.getRemoteDevice(address);

        if (device == null) {
            Log.println(Log.ERROR, "fury", "Null bt device");
        }

        try {
            btSocket = createBluetoothSocket(device);
        } catch (IOException e) {
            Toast.makeText(getBaseContext(), "Socket creation failed", Toast.LENGTH_LONG).show();
            Log.println(Log.ERROR, "fury", "Socket failed");
        }

        // Establish the Bluetooth socket connection.
        try {
            btSocket.connect();
            Toast.makeText(getBaseContext(), "Connected", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            try {
                e.printStackTrace();
                btSocket.close();
            } catch (IOException e2) {
                //insert code to deal with this
            }
        }
        mConnectedThread = new ConnectedThread(btSocket);
        mConnectedThread.start();
    }

    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {

        return device.createRfcommSocketToServiceRecord(myUUID);
        //creates secure outgoing connection with BT device using UUID
    }

    @Override
    public void onResume() {

        if (!btSocket.isConnected()) {
            Log.println(Log.INFO, "Resume", "LED");
            connectBt();
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            //Don't leave Bluetooth sockets open when leaving activity
            btSocket.close();
        } catch (IOException e2) {
            //insert code to deal with this
        }
    }

    private void sendSignal(String number) {
        if (mConnectedThread != null) {
            mConnectedThread.write(number);
        }
    }

    private void msg(String s) {
        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
    }


    //create new class for connect thread
    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        //creation of the connect thread
        public ConnectedThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                //Create I/O streams for connection
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }


        public void run() {
            byte[] buffer = new byte[1024];
            int begin = 0;
            int bytes = 0;
            while (true) {
                try {
                    bytes += mmInStream.read(buffer, bytes, buffer.length - bytes);
                    for (int i = begin; i < bytes; i++) {
//                        Log.println(Log.INFO, "bytes",String.valueOf(bytes));
                        if (buffer[i] == "#".getBytes()[0]) {
                            Log.println(Log.INFO, "i = ", String.valueOf(i));
                            handler.obtainMessage(handlerState, begin, i, buffer).sendToTarget();
                            begin = 0;
                            bytes = 0;
                        }
                    }
                } catch (IOException e) {
                    break;
                }
            }
        }

        //write method
        public void write(String input) {
            byte[] msgBuffer = input.getBytes();           //converts entered String into bytes
            try {
                mmOutStream.write(msgBuffer);                //write bytes over
                // BT connection via outstream
            } catch (IOException e) {
                //if you cannot write, close the application
                Toast.makeText(getBaseContext(), "Connection Failure", Toast.LENGTH_LONG).show();
                finish();

            }
        }
    }
}