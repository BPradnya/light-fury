/**
 * @file
 *
 * The purpose of this "C" callbacks is to provide the code to be able
 * to call pure C functions and unit-test it in C test framework
 */

#include <stdint.h>
#include <stdbool.h>
#include "FreeRTOS.h"
#include "semphr.h"
#include "stddef.h"
#include "c_period_callbacks.h"
#include "watchdog.h"
#include "string.h"
#include "BlueTooth.h"
#include "printf_lib.h"
#include "can.h"
#include "Can_Operation.h"
#include "Can_busOff_rem.h"
#include "c_geo_bridge_communication.h"

static char writeQue[RXQSIZE];   //should be 100

bool dbc_app_send_can_msg(uint32_t mid, uint8_t dlc, uint8_t bytes[8])
{
    can_msg_t can_msg = { 0 };
    can_msg.msg_id                = mid;
    can_msg.frame_fields.data_len = dlc;
    memcpy(can_msg.data.bytes, bytes, dlc);
    return CAN_tx(can1, &can_msg, 0);
}

bool C_period_init(void)
{
    bool flag = false;
    if(CAN_init(can1,100,32,32,(void*)bus_off_cb,NULL))
    {
        CAN_bypass_filter_accept_all_msgs();
        CAN_reset_bus(can1);

        flag = true;
    }

    if(initUart())
    {
        flag = true;
    }

    else
    {
        flag = false;
    }

    return flag;
}

bool C_period_reg_tlm(void) {
    return true;
}

void C_period_1Hz(uint32_t count)
{
    (void) count;

    if(CAN_is_bus_off(can1))
    {
        bus_off_cb(can1);
    }
}

void C_period_10Hz(uint32_t count)
{
    (void) count;
    char *geo = NULL;

    if(fetchGeoData())
    {
        geo =  parseGeoData();
        //u0_dbg_printf("The compass data is %s\n", geo);
    }



    if(geo)
    {
        //u0_dbg_printf("in gio");
        //u0_dbg_printf("%s\n",geo);
        xBee_transmit(geo,0);
    }

    if(xBee_receive(writeQue,sizeof(writeQue),0))
    {
       Can_tx(can1,writeQue);
    }
}

void C_period_100Hz(uint32_t count) {
    (void) count;
    can_HB_tx(can1);
}

void C_period_1000Hz(uint32_t count) {
    (void) count;
}
