/*
 * c_ultrasonic.h
 *
 *  Created on: Mar 30, 2019
 *      Author: prita
 */

#ifndef C_ULTRASONIC_H_
#define C_ULTRASONIC_H_
#ifdef __cplusplus
extern "C" {
#endif

typedef enum{
    left,
    front,
    right,
    rear
}SENSOR_POS;

void ultrasonic_init(void);
void trigger_ultrasonic_sensor(SENSOR_POS );
void enable_external_interrupt(uint8_t pin_num);



#ifdef __cplusplus
}
#endif
#endif /* C_ULTRASONIC_H_ */
