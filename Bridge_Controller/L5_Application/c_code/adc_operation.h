/*
 * adc_operation.h
 *
 *  Created on: Apr 3, 2019
 *      Author: kaust
 */

#ifndef ADC_OPERATION_H_
#define ADC_OPERATION_H_

#include "adc0.h"

void initialize_adc();
uint16_t adc_getDistance();



#endif /* ADC_OPERATION_H_ */
