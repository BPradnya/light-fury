/*
 * Can_tx.c
 *
 *  Created on: Mar 5, 2019
 *      Author: kaust
 */

#include "Can_Operation.h"
#include <stdbool.h>
#include "printf_lib.h"

//extern

bool can_HB_tx(can_t can)
{
    LF_BRIDGE_HB_t bridgeHeartbeat = {1};

//    bridgeHeartbeat.bridge_hb = 1;
    dbc_encode_and_send_LF_BRIDGE_HB(&bridgeHeartbeat);

    return true;
}

bool Can_tx(can_t can, char *locationData)
{
    LF_bridge_t location_data = {0};
    uint8_t dataInt = atoi(locationData);
    location_data.dest_lat_long_unsigned = dataInt;
    //location_data.dist_to_next_point_float_signed = 0x15;
    dbc_encode_and_send_LF_bridge(&location_data);

    //printf("%d\n",dataInt);

    return true;
}

const uint32_t                             LF_DRIVER_HB__MIA_MS = 3000;
const LF_DRIVER_HB_t                       LF_DRIVER_HB__MIA_MSG = {0};
const uint32_t                             LF_GEO_BT_COMP__MIA_MS = 3000;
const LF_GEO_BT_COMP_t                     LF_GEO_BT_COMP__MIA_MSG = {0};
const uint32_t                             LF_GEO_BT_LAT_LONG__MIA_MS = 3000;
const LF_GEO_BT_LAT_LONG_t                 LF_GEO_BT_LAT_LONG__MIA_MSG = {0};

void Can_rx_HB(can_t can)
{
    LF_bridge_t driverStatusMsg;
    can_msg_t can_msg;

    if(CAN_rx(can,&can_msg,0))
    {
        dbc_msg_hdr_t can_msg_hdr;

        can_msg_hdr.dlc = can_msg.frame_fields.data_len;
        can_msg_hdr.mid = can_msg.msg_id;

        dbc_decode_LF_DRIVER_HB(&driverStatusMsg,can_msg.data.bytes, &can_msg_hdr);
    }

    bool check_mia;
    check_mia = dbc_handle_mia_LF_DRIVER_HB(&driverStatusMsg, 10);
}

LF_GEO_BT_COMP_t Can_comp_rx(can_t can)
{
    LF_GEO_BT_COMP_t compData;

    can_msg_t can_msg;
    //u0_dbg_printf("The data is in compass\n");

    while(CAN_rx(can,&can_msg,0))
    {
        //u0_dbg_printf("The data is \n");
        dbc_msg_hdr_t can_msg_hdr;

        can_msg_hdr.dlc = can_msg.frame_fields.data_len;
        can_msg_hdr.mid = can_msg.msg_id;

        if(dbc_decode_LF_GEO_BT_COMP(&compData,can_msg.data.bytes, &can_msg_hdr))
        {
            //u0_dbg_printf("The data is in compass\n");
            u0_dbg_printf("The data is  %u\n", compData.GEO_BT_comp_debug);
            return compData;
        }
    }

    bool check_mia = false;
    check_mia = dbc_handle_mia_LF_GEO_BT_COMP(&compData, 10);

    if(check_mia)
    {
        return compData;
    }

    return compData;

}

LF_GEO_BT_LAT_LONG_t Can_geo_rx(can_t can)
{
    LF_GEO_BT_LAT_LONG_t geoData;

    can_msg_t can_msg;
    //u0_dbg_printf("The data is \n");

    while(CAN_rx(can,&can_msg,0))
    {
        //u0_dbg_printf("The data is in long\n");
        dbc_msg_hdr_t can_msg_hdr;

        can_msg_hdr.dlc = can_msg.frame_fields.data_len;
        can_msg_hdr.mid = can_msg.msg_id;

        if(dbc_decode_LF_GEO_BT_LAT_LONG(&geoData,can_msg.data.bytes, &can_msg_hdr))
        {
            //u0_dbg_printf("The data is in compass\n");
            u0_dbg_printf("The data is  %f\n", geoData.GEO_BT_lat_debug);
            u0_dbg_printf("The data is  %f\n", geoData.GEO_BT_long_debug);
            return geoData;
        }
    }


    bool check_mia = false;
    check_mia = dbc_handle_mia_LF_GEO_BT_LAT_LONG(&geoData, 10);

    if(check_mia)
    {
        return geoData;
    }

    return geoData;
}
