/*
 * c_geo_bridge_communication.h
 *
 *  Created on: Apr 24, 2019
 *      Author: kaust
 */

#ifndef C_GEO_BRIDGE_COMMUNICATION_H_
#define C_GEO_BRIDGE_COMMUNICATION_H_

#include <stdbool.h>
#include <Can_operation.h>

bool fetchGeoData();
char* parseGeoData();

#endif /* C_GEO_BRIDGE_COMMUNICATION_H_ */
