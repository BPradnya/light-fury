/*
 * BlueTooth.h
 *
 *  Created on: Mar 21, 2019
 *      Author: kaust
 */

#ifndef BLUETOOTH_H_
#define BLUETOOTH_H_

#ifdef __cplusplus
extern "C"
{
#endif
#include <stdbool.h>

#define RXQSIZE  100

bool initUart(void);
void BT_transmit(char* pBuff, unsigned int timeout);
bool BT_receive(char* pBuff, int maxLen, unsigned int timeout);

void xBee_transmit(char* pBuff, unsigned int timeout);
bool xBee_receive(char* pBuff, int maxLen, unsigned int timeout);
#ifdef __cplusplus
}
#endif


#endif /* BLUETOOTH_H_ */
