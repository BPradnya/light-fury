/**
 * @file
 * This is a C header file that other C code can use to access UART2
 * driver that is written in C++
 *
 * Note that we have a C header file, but its implementation in C++
 * such that we can invoke C++ Uart driver
 *
 * The C Unit-test framework can use this header file to "Mock" the
 * UART2 API and carry out the tests.
 */
#ifndef C_UART_H_
#define C_UART_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>

void uart2_putline(const char* pBuff, unsigned int timeout);
bool uart2_init(int baudRate, int rxQsize);
bool uart2_gets(char* pBuff, int maxLen, unsigned int timeout);
bool uart2_getChar(char* pInputChar, unsigned int timeout);

void uart3_putline(const char* pBuff, unsigned int timeout);
bool uart3_init(int baudRate, int rxQsize);
bool uart3_gets(char* pBuff, int maxLen, unsigned int timeout);
bool uart3_getChar(char* pInputChar, unsigned int timeout);

#ifdef __cplusplus
}
#endif

#endif /* C_UART_H_ */
