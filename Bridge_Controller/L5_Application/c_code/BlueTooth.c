/*
 * BlueTooth.cpp
 *
 *  Created on: Apr 13, 2019
 *      Author: kaust
 */

#include "BlueTooth.h"
#include "c_uart.h"
#include <stdio.h>
#include <stdint.h>


bool initUart(void)
{
    uint16_t baudRate = 9600;

    if(uart2_init(baudRate,RXQSIZE) && uart3_init(baudRate,RXQSIZE))
    {
        return true;
    }

    return false;
}

bool BT_receive(char* pBuff, int maxLen, unsigned int timeout)
{
    return uart2_gets(pBuff,maxLen,timeout);
}

bool xBee_receive(char* pBuff, int maxLen, unsigned int timeout)
{
    return uart3_gets(pBuff,maxLen,timeout);
}

void BT_transmit(char* pBuff, unsigned int timeout)
{
    uart2_putline(pBuff,timeout);
}

void xBee_transmit(char* pBuff, unsigned int timeout)
{
    uart3_putline(pBuff,timeout);
}
