/*
 * c_geo_bridge_communication.c
 *
 *  Created on: Apr 24, 2019
 *      Author: kaust
 */

#include "c_geo_bridge_communication.h"
#include "printf_lib.h"
#include "string.h"

LF_GEO_BT_COMP_t compData = {0};
LF_GEO_BT_LAT_LONG_t geoData = {0};
static char geo[100] = {0};

bool fetchGeoData()
{
    geoData = Can_geo_rx(can1);
   compData = Can_comp_rx(can1);  //geoData = Can_rx(can1);

   //u0_dbg_printf("The data is %d\n", geoData.GEO_BT_lat_debug);
   return true;
}

char* parseGeoData()
{
    memset(geo, '\0', sizeof(geo));
    sprintf(geo,"%.6f,%.6f,%u", geoData.GEO_BT_lat_debug, geoData.GEO_BT_long_debug, compData.GEO_BT_comp_debug);  //sprintf( geo, "%f", compass);


    return geo;
}
