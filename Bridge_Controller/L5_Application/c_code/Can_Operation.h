/*
 * Can_tx.h
 *
 *  Created on: Mar 5, 2019
 *      Author: kaust
 */

#ifndef CAN_OPERATION_H_
#define CAN_OPERATION_H_

#include "can.h"
#include "switch_led.h"
#include "../../_can_dbc/generated_can.h"

bool can_HB_tx(can_t can);
bool Can_tx(can_t can, char *locationData);
void Can_HB_rx(can_t can);
LF_GEO_BT_COMP_t Can_comp_rx(can_t can);
LF_GEO_BT_LAT_LONG_t Can_geo_rx(can_t can);

#endif /* CAN_OPERATION_H_ */
