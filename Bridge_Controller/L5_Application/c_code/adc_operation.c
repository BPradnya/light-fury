/*
 * adc_operation.c
 *
 *  Created on: Apr 3, 2019
 *      Author: kaust
 */

#include "LPC17xx.h"
#include <stdbool.h>
#include "adc_operation.h"


void initialize_adc()
{
    LPC_PINCON->PINSEL1 &= ~(3<<20);
    LPC_PINCON->PINSEL1 |= (1<<20);
    adc0_init();
}

uint16_t adc_getDistance()
{
    uint16_t dist = 0;
    float Vout = 0;
    const float a = 21.715; // y = a/x+b;
    const float b = 0.1285;
    uint16_t vo = adc0_get_reading(3);
    //printf("vo = %d\n",vo);
    Vout = (3.3*vo/4096.0);
    //printf("Vout = %f\n",Vout);
    if(Vout > b)
    {
        dist = a/(Vout-b);
    }

    return dist;
}
